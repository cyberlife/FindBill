/*
 * Copyright 2016 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.demo;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.ImageReader.OnImageAvailableListener;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Future;

import org.tensorflow.demo.OverlayView.DrawCallback;
import org.tensorflow.demo.env.BorderedText;
import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.ethereum.ClientManager;
import org.tensorflow.demo.ethereum.CommonToken;
import org.tensorflow.demo.ethereum.Stickers;
import org.tensorflow.demo.objects.CollectiblePopUp;
import org.tensorflow.demo.tracking.MultiBoxTracker;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.core.methods.response.TransactionReceipt;

import static org.web3j.tx.Contract.GAS_LIMIT;
import static org.web3j.tx.ManagedTransaction.GAS_PRICE;


/**
 * An activity that uses a TensorFlowMultiBoxDetector and ObjectTracker to detect and then track
 * objects.
 */
public class DetectorActivity extends CameraActivity implements OnImageAvailableListener {
    private static final Logger LOGGER = new Logger();

    // Configuration values for the prepackaged multibox model.

    private static final int MB_INPUT_SIZE = 224;
    private static final int MB_IMAGE_MEAN = 128;
    private static final float MB_IMAGE_STD = 128;
    private static final String MB_INPUT_NAME = "ResizeBilinear";
    private static final String MB_OUTPUT_LOCATIONS_NAME = "output_locations/Reshape";
    private static final String MB_OUTPUT_SCORES_NAME = "output_scores/Reshape";
    private static final String MB_MODEL_FILE = "file:///android_asset/multibox_model.pb";
    private static final String MB_LOCATION_FILE =
            "file:///android_asset/multibox_location_priors.txt";

    private static final int TF_OD_API_INPUT_SIZE = 300;
    private static final String TF_OD_API_MODEL_FILE =
            "file:///android_asset/ssd_mobilenet_v1_android_export.pb";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/coco_labels_list.txt";

    // Which detection model to use: by default uses Tensorflow Object Detection API frozen
    // checkpoints.  Optionally use legacy Multibox (trained using an older version of the API)
    // or YOLO.
    private enum DetectorMode {
        TF_OD_API, MULTIBOX, YOLO;
    }

    private static final DetectorMode MODE = DetectorMode.TF_OD_API;

    // Minimum detection confidence to track a detection.
    private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.90f;
    private static final float MINIMUM_CONFIDENCE_MULTIBOX = 0.90f;
    private static final float MINIMUM_CONFIDENCE_YOLO = 0.90f;

    private static final boolean MAINTAIN_ASPECT = MODE == DetectorMode.YOLO;

    private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 680);

    private static final boolean SAVE_PREVIEW_BITMAP = false;
    private static final float TEXT_SIZE_DIP = 10;

    private Integer sensorOrientation;

    private Classifier detector;

    private long lastProcessingTimeMs;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private Bitmap cropCopyBitmap = null;

    private boolean computingDetection = false;

    private long timestamp = 0;

    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;

    private MultiBoxTracker tracker;

    private byte[] luminanceCopy;

    private BorderedText borderedText;

    private MintEligible mintEligibleTask;

    private int popupBeingShown = 0;

    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {
        final float textSizePx =
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        borderedText.setTypeface(Typeface.MONOSPACE);

        tracker = new MultiBoxTracker(this);

        int cropSize = TF_OD_API_INPUT_SIZE;
        if (MODE == DetectorMode.MULTIBOX) {
            detector =
                    TensorFlowMultiBoxDetector.create(
                            getAssets(),
                            MB_MODEL_FILE,
                            MB_LOCATION_FILE,
                            MB_IMAGE_MEAN,
                            MB_IMAGE_STD,
                            MB_INPUT_NAME,
                            MB_OUTPUT_LOCATIONS_NAME,
                            MB_OUTPUT_SCORES_NAME);
            cropSize = MB_INPUT_SIZE;
        } else {
            try {
                detector = TensorFlowObjectDetectionAPIModel.create(
                        getAssets(), TF_OD_API_MODEL_FILE, TF_OD_API_LABELS_FILE, TF_OD_API_INPUT_SIZE);
                cropSize = TF_OD_API_INPUT_SIZE;
            } catch (final IOException e) {
                LOGGER.e("Exception initializing classifier!", e);
                Toast toast =
                        Toast.makeText(
                                getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
                toast.show();
                finish();
            }
        }

        previewWidth = size.getWidth();
        previewHeight = size.getHeight();

        Log.e("bill", "detector activity sizes: " + previewHeight + " " + previewWidth);

        sensorOrientation = rotation - getScreenOrientation();
        LOGGER.i("Camera orientation relative to screen canvas: %d", sensorOrientation);

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Config.ARGB_8888);
        croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);

        trackingOverlay = (OverlayView) findViewById(R.id.tracking_overlay);
        trackingOverlay.addCallback(
                new DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        tracker.draw(canvas);
                        if (isDebug()) {
                            tracker.drawDebug(canvas);
                        }
                    }
                });

        addCallback(
                new DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        if (!isDebug()) {
                            return;
                        }
                        final Bitmap copy = cropCopyBitmap;
                        if (copy == null) {
                            return;
                        }

                        final int backgroundColor = Color.argb(100, 0, 0, 0);
                        canvas.drawColor(backgroundColor);

                        final Matrix matrix = new Matrix();
                        final float scaleFactor = 2;
                        matrix.postScale(scaleFactor, scaleFactor);
                        matrix.postTranslate(
                                canvas.getWidth() - copy.getWidth() * scaleFactor,
                                canvas.getHeight() - copy.getHeight() * scaleFactor);
                        canvas.drawBitmap(copy, matrix, new Paint());

                        final Vector<String> lines = new Vector<String>();
                        if (detector != null) {
                            final String statString = detector.getStatString();
                            final String[] statLines = statString.split("\n");
                            for (final String line : statLines) {
                                lines.add(line);
                            }
                        }
                        lines.add("");

                        lines.add("Frame: " + previewWidth + "x" + previewHeight);
                        lines.add("Crop: " + copy.getWidth() + "x" + copy.getHeight());
                        lines.add("View: " + canvas.getWidth() + "x" + canvas.getHeight());
                        lines.add("Rotation: " + sensorOrientation);
                        lines.add("Inference time: " + lastProcessingTimeMs + "ms");

                        borderedText.drawLines(canvas, 10, canvas.getHeight() - 10, lines);
                    }
                });
    }

    OverlayView trackingOverlay;

    @Override
    protected void processImage() {

        ++timestamp;
        final long currTimestamp = timestamp;
        byte[] originalLuminance = getLuminance();
        tracker.onFrame(
                previewWidth,
                previewHeight,
                getLuminanceStride(),
                sensorOrientation,
                originalLuminance,
                timestamp);
        trackingOverlay.postInvalidate();

        // No mutex needed as this method is not reentrant.
        if (computingDetection) {
            readyForNextImage();
            return;
        }
        computingDetection = true;
        LOGGER.i("Preparing image " + currTimestamp + " for detection in bg thread.");

        rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

        if (luminanceCopy == null) {
            luminanceCopy = new byte[originalLuminance.length];
        }
        System.arraycopy(originalLuminance, 0, luminanceCopy, 0, originalLuminance.length);
        readyForNextImage();

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
        // For examining the actual TF input.
        if (SAVE_PREVIEW_BITMAP) {
            ImageUtils.saveBitmap(croppedBitmap);
        }

        runInBackground(
                new Runnable() {
                    @Override
                    public void run() {
                        LOGGER.i("Running detection on image " + currTimestamp);
                        final long startTime = SystemClock.uptimeMillis();
                        final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);
                        lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

                        cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
                        final Canvas canvas = new Canvas(cropCopyBitmap);
                        final Paint paint = new Paint();
                        paint.setColor(Color.RED);
                        paint.setStyle(Style.STROKE);
                        paint.setStrokeWidth(2.0f);

                        float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
                        switch (MODE) {
                            case TF_OD_API:
                                minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
                                break;
                            case MULTIBOX:
                                minimumConfidence = MINIMUM_CONFIDENCE_MULTIBOX;
                                break;
                            case YOLO:
                                minimumConfidence = MINIMUM_CONFIDENCE_YOLO;
                                break;
                        }

                        final List<Classifier.Recognition> mappedRecognitions =
                                new LinkedList<Classifier.Recognition>();

                        for (final Classifier.Recognition result : results) {
                            final RectF location = result.getLocation();
                            if (location != null && result.getConfidence() >= minimumConfidence) {
                                canvas.drawRect(location, paint);

                                cropToFrameTransform.mapRect(location);
                                result.setLocation(location);
                                mappedRecognitions.add(result);
                            }
                        }

                        if (!alreadyDetected.toString().toLowerCase().contains(results.get(0).getTitle().toString().toLowerCase())
                                && results.get(0).getConfidence() >= MINIMUM_CONFIDENCE_MULTIBOX
                                ) {

                            alreadyDetected.append("_" + results.get(0).getTitle().toString() + "_");

                            if (random_object.equals(results.get(0).getTitle().toString().toLowerCase())) {

                                currentlyFound++;

                                if (currentlyFound >= difficulty) {



                                } else {

                                    randomizeObject();

                                }

                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    int underlines = alreadyDetected.toString().length() -
                                            alreadyDetected.toString().replace("_", "").length();

                                    Random r = new Random();

                                    int chanceToGetCollectible = r.nextInt(10);

                                    Log.e("bill", "chance: " + chanceToGetCollectible + " " + underlines);

                                    if (underlines >= 8 && popupBeingShown == 0) {

                                        mintEligibleTask = new MintEligible(results.get(0).getTitle().toString());

                                        mintEligibleTask.execute();

                                    }

                                }
                            });

                            Log.e("bill", alreadyDetected.toString());

                        }

                        tracker.trackResults(mappedRecognitions, luminanceCopy, currTimestamp);
                        trackingOverlay.postInvalidate();

                        requestRender();
                        computingDetection = false;

                    }
                });
    }

    private class MintEligible extends AsyncTask<Void, Void, CollectiblePopUp> {

        private String objectHash;

        public MintEligible(String objectHash) {

            this.objectHash = objectHash;

        }

        @Override
        protected CollectiblePopUp doInBackground(Void... params) {

            SharedPreferences prefs = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);

            String address = prefs.getString("publicAddress", null);

            String objectName = getNameFromHash(objectHash);

            Stickers stickers = Stickers.load(
                    "0x73b5f9f4f6859ab20d72511b061fd699c15a2e07", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            CommonToken common = CommonToken.load(
                    "0x0e5915ff0b18b68c0dd35f9d49c76579f84ff39b", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            try {

                Log.e("bill", String.valueOf(stickers.isValid()));

                BigInteger id = stickers.getIdFromName(objectName).send();

                boolean collectibleExists = stickers.exists(id).send();

                BigInteger iouBalance = common.getMaxToBeMinted(address).send();

                Log.e("bill", String.valueOf(iouBalance.intValue()));

                BigInteger objectPrice = BigInteger.valueOf(5);

                if (!collectibleExists && iouBalance.intValue() >= objectPrice.intValue() && getImage(objectHash) != 0) {

                    popupBeingShown = 1;

                    return new CollectiblePopUp(objectName, objectHash, objectPrice.intValue());

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(CollectiblePopUp result) {

            if (result != null) {

                showCollectibleDialog(result.getObjectName(), "New fancy object!",
                        result.getObjectHash(), result.getObjectPrice());

            }


        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }

    private class AssignCollectible extends AsyncTask<Void, Void, Void> {

        private String objectHash, objectName;
        private int price;

        public AssignCollectible(String objectHash, String objectName, int price) {

            this.objectHash = objectHash;
            this.objectName = objectName;
            this.price = price;

        }

        @Override
        protected Void doInBackground(Void... params) {

            SharedPreferences prefs = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);

            String address = prefs.getString("publicAddress", null);

            Stickers stickers = Stickers.load(
                    "0x73b5f9f4f6859ab20d72511b061fd699c15a2e07", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            ClientManager client = ClientManager.load(
                    "0xe3c0398b4127dff2cc396a09d243341d6bf33785", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            try {

                BigInteger id = stickers.getIdFromName(objectName).send();

                Future<TransactionReceipt> paymentReceipt = client.
                            mintAndTransfer(address, "0x0", BigInteger.valueOf(price)).sendAsync();

                Future<TransactionReceipt> stickerReceipt = stickers.mint(address, id).sendAsync();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }

    @Override
    public synchronized void onStop() {

        if (mintEligibleTask != null) {

            mintEligibleTask.cancel(true);

        }

        super.onStop();

    }

    private void showCollectibleDialog(String name, String objectDescription, String objectHash, int price) {

        int image = getImage(objectHash);

        String formattedName = name.replace("_"," ");

        new MaterialStyledDialog.Builder(this)
                .setTitle(formattedName.substring(0, 1).toUpperCase() + formattedName.substring(1) + " at " + price + " tokens")
                .setDescription(objectDescription)
                .setStyle(com.github.javiersantos.materialstyleddialogs.enums.Style.HEADER_WITH_ICON)
                .setIcon(image)
                .withDialogAnimation(true)
                .setHeaderColor(R.color.primary)
                .setCancelable(false)

                .setPositiveText("Buy it!")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        new AssignCollectible(objectHash, name, price).execute();

                        popupBeingShown = 0;

                    }
                })

                .setNegativeText("Skip it")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        popupBeingShown = 0;

                    }
                })

                .show();


    }

    private String getNameFromHash(String object) {

        StringBuilder imageName = new StringBuilder();

        if (object.equals("9d982f997727cc88f234d5061a467218")) {

            imageName.append("banana");

        } else if (object.equals("bbbbf896e85ecd352133194a4ab039f4")) {

            imageName.append("bottle");

        } else if (object.equals("19e3e84f0d7c5b1ad6ea6d3f67a83b3a")) {

            imageName.append("fridge");

        } else if (object.equals("f6911f1aa9c68414c9817f32241ad811")) {

            imageName.append("laptop");

        } else if (object.equals("8061dd029ea77c5a0c41faeb7616fafc")) {

            imageName.append("microwave");

        } else if (object.equals("f529eb89eaab055209283814459317be")) {

            imageName.append("oven");

        } else if (object.equals("da460f221de56adfed434aeac3f181dc")) {

            imageName.append("teddy_bear");

        } else if (object.equals("f30f2be89f530970f0f8444e1ad7365f")) {

            imageName.append("wine");

        }

        return imageName.toString();

    }

    private int getImage(String object) {

        int image = 0;

        if (object.equals("9d982f997727cc88f234d5061a467218")) {

            image = R.drawable.banana;

        } else if (object.equals("bbbbf896e85ecd352133194a4ab039f4")) {

            image = R.drawable.bottle;

        } else if (object.equals("19e3e84f0d7c5b1ad6ea6d3f67a83b3a")) {

            image = R.drawable.fridge;

        } else if (object.equals("f6911f1aa9c68414c9817f32241ad811")) {

            image = R.drawable.laptop;

        } else if (object.equals("8061dd029ea77c5a0c41faeb7616fafc")) {

            image = R.drawable.microwave;

        } else if (object.equals("f529eb89eaab055209283814459317be")) {

            image = R.drawable.oven;

        } else if (object.equals("da460f221de56adfed434aeac3f181dc")) {

            image = R.drawable.teddy_bear;

        } else if (object.equals("f30f2be89f530970f0f8444e1ad7365f")) {

            image = R.drawable.wine;

        }

        return image;

    }

    private void giveCollectible(String object) {

        Log.e("bill", "giving collectible");

        int image = getImage(object);

        fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);

        collectible_image.setImageResource(image);

        collectible_image.setVisibility(View.VISIBLE);

        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.got_item_sound);
        mp.start();

        collectible_image.startAnimation(fadeIn);

        collectible_image.setVisibility(View.INVISIBLE);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.camera_connection_fragment_tracking;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    @Override
    public void onSetDebug(final boolean debug) {
        detector.enableStatLogging(debug);
    }

}

