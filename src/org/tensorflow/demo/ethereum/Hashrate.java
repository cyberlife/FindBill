package org.tensorflow.demo.ethereum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.4.0.
 */
public class Hashrate extends Contract {
    private static final String BINARY = "0x606060405262093a8060025562015180600355341561001d57600080fd5b604051602080610ba383398101604052808051906020019091905050336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555080600a60006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506002544201600681905550600354420160078190555050610ac3806100e06000396000f3006060604052600436106100c5576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680630d01ebb0146100ca57806320d31909146100f35780633235f04f146101085780634d3fc5de1461012b5780636495605114610154578063715018a6146101695780637e56798b1461017e5780638da5cb5b146101a75780639c26a3a4146101fc578063e9b29a9d14610225578063f056f6431461025e578063f2fde38b14610297578063f921e484146102d0575b600080fd5b34156100d557600080fd5b6100dd6102f3565b6040518082815260200191505060405180910390f35b34156100fe57600080fd5b6101066102fd565b005b341561011357600080fd5b61012960048080359060200190919050506103b1565b005b341561013657600080fd5b61013e61047d565b6040518082815260200191505060405180910390f35b341561015f57600080fd5b610167610487565b005b341561017457600080fd5b61017c61053b565b005b341561018957600080fd5b61019161063d565b6040518082815260200191505060405180910390f35b34156101b257600080fd5b6101ba610647565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561020757600080fd5b61020f61066c565b6040518082815260200191505060405180910390f35b341561023057600080fd5b61025c600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610676565b005b341561026957600080fd5b610295600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610715565b005b34156102a257600080fd5b6102ce600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610817565b005b34156102db57600080fd5b6102f1600480803590602001909190505061087e565b005b6000600554905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561035857600080fd5b600754421015151561036957600080fd5b60006005819055506003544201600781905550426009819055507ff062be354e081ea68cde443fe62fe5861d55bdd634ae2e304d6baa29f2ed997f60405160405180910390a1565b600a60009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561040d57600080fd5b6104228160045461098190919063ffffffff16565b60048190555061043d8160055461098190919063ffffffff16565b6005819055507fd4cc53cee9d22ccbfc9717726684335791ae94f48ceb5cb5ee125502651d6181816040518082815260200191505060405180910390a150565b6000600454905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156104e257600080fd5b60065442101515156104f357600080fd5b60006004819055506002544201600681905550426008819055507f26994db84ed9443845503eae1fdf66f36a52f2bf7e520006778e970b145a2ae160405160405180910390a1565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561059657600080fd5b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482060405160405180910390a260008060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550565b6000600954905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6000600854905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156106d157600080fd5b80600a60006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561077057600080fd5b600160008273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615600160008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff02191690831515021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561087257600080fd5b61087b8161099d565b50565b60011515600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615151415156108dd57600080fd5b6108f28160045461098190919063ffffffff16565b60048190555061090d8160055461098190919063ffffffff16565b6005819055507f4ecfec82fd4c2a9add9fcea0d771bc776ba1fe3a03afb4e199815438fbad020f3382604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018281526020019250505060405180910390a150565b6000818301905082811015151561099457fe5b80905092915050565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff16141515156109d957600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a3806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550505600a165627a7a72305820428559c4f1208709f6d85eb60096b9931708f4eee53f02c6cb73c6c59b217c1a0029";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final String FUNC_RESTARTLONG = "restartLong";

    public static final String FUNC_RESTARTSHORT = "restartShort";

    public static final String FUNC_SETAPI = "setAPI";

    public static final String FUNC_SETCLIENTHASHPROVENANCE = "setClientHashProvenance";

    public static final String FUNC_ADDAPIHASH = "addAPIHash";

    public static final String FUNC_ADDCLIENTSIDEHASH = "addClientSideHash";

    public static final String FUNC_GETLONGTERMHASHRATE = "getLongTermHashRate";

    public static final String FUNC_GETSHORTTERMHASHRATE = "getShortTermHashRate";

    public static final String FUNC_LASTLONGRESTART = "lastLongRestart";

    public static final String FUNC_LASTSHORTRESTART = "lastShortRestart";

    public static final Event RESTARTLONG_EVENT = new Event("RestartLong", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event RESTARTSHORT_EVENT = new Event("RestartShort", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event APIHASHES_EVENT = new Event("APIHashes", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
    ;

    public static final Event CLIENTSIDEHASH_EVENT = new Event("ClientSideHash", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event OWNERSHIPRENOUNCED_EVENT = new Event("OwnershipRenounced", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
    }

    protected Hashrate(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Hashrate(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String _newOwner) {
        final Function function = new Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new Address(_newOwner)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<Hashrate> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String _api) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Address(_api)));
        return deployRemoteCall(Hashrate.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static RemoteCall<Hashrate> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String _api) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Address(_api)));
        return deployRemoteCall(Hashrate.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public List<RestartLongEventResponse> getRestartLongEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(RESTARTLONG_EVENT, transactionReceipt);
        ArrayList<RestartLongEventResponse> responses = new ArrayList<RestartLongEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            RestartLongEventResponse typedResponse = new RestartLongEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<RestartLongEventResponse> restartLongEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, RestartLongEventResponse>() {
            @Override
            public RestartLongEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(RESTARTLONG_EVENT, log);
                RestartLongEventResponse typedResponse = new RestartLongEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<RestartLongEventResponse> restartLongEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(RESTARTLONG_EVENT));
        return restartLongEventObservable(filter);
    }

    public List<RestartShortEventResponse> getRestartShortEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(RESTARTSHORT_EVENT, transactionReceipt);
        ArrayList<RestartShortEventResponse> responses = new ArrayList<RestartShortEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            RestartShortEventResponse typedResponse = new RestartShortEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<RestartShortEventResponse> restartShortEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, RestartShortEventResponse>() {
            @Override
            public RestartShortEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(RESTARTSHORT_EVENT, log);
                RestartShortEventResponse typedResponse = new RestartShortEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<RestartShortEventResponse> restartShortEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(RESTARTSHORT_EVENT));
        return restartShortEventObservable(filter);
    }

    public List<APIHashesEventResponse> getAPIHashesEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(APIHASHES_EVENT, transactionReceipt);
        ArrayList<APIHashesEventResponse> responses = new ArrayList<APIHashesEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            APIHashesEventResponse typedResponse = new APIHashesEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.howMany = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<APIHashesEventResponse> aPIHashesEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, APIHashesEventResponse>() {
            @Override
            public APIHashesEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(APIHASHES_EVENT, log);
                APIHashesEventResponse typedResponse = new APIHashesEventResponse();
                typedResponse.log = log;
                typedResponse.howMany = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<APIHashesEventResponse> aPIHashesEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(APIHASHES_EVENT));
        return aPIHashesEventObservable(filter);
    }

    public List<ClientSideHashEventResponse> getClientSideHashEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(CLIENTSIDEHASH_EVENT, transactionReceipt);
        ArrayList<ClientSideHashEventResponse> responses = new ArrayList<ClientSideHashEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            ClientSideHashEventResponse typedResponse = new ClientSideHashEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.client = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.howMany = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ClientSideHashEventResponse> clientSideHashEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, ClientSideHashEventResponse>() {
            @Override
            public ClientSideHashEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(CLIENTSIDEHASH_EVENT, log);
                ClientSideHashEventResponse typedResponse = new ClientSideHashEventResponse();
                typedResponse.log = log;
                typedResponse.client = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.howMany = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<ClientSideHashEventResponse> clientSideHashEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(CLIENTSIDEHASH_EVENT));
        return clientSideHashEventObservable(filter);
    }

    public List<OwnershipRenouncedEventResponse> getOwnershipRenouncedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, transactionReceipt);
        ArrayList<OwnershipRenouncedEventResponse> responses = new ArrayList<OwnershipRenouncedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipRenouncedEventResponse>() {
            @Override
            public OwnershipRenouncedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, log);
                OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPRENOUNCED_EVENT));
        return ownershipRenouncedEventObservable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventObservable(filter);
    }

    public RemoteCall<TransactionReceipt> restartLong() {
        final Function function = new Function(
                FUNC_RESTARTLONG, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> restartShort() {
        final Function function = new Function(
                FUNC_RESTARTSHORT, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> setAPI(String _api) {
        final Function function = new Function(
                FUNC_SETAPI, 
                Arrays.<Type>asList(new Address(_api)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> setClientHashProvenance(String _client) {
        final Function function = new Function(
                FUNC_SETCLIENTHASHPROVENANCE, 
                Arrays.<Type>asList(new Address(_client)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> addAPIHash(BigInteger quantity) {
        final Function function = new Function(
                FUNC_ADDAPIHASH, 
                Arrays.<Type>asList(new Uint256(quantity)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> addClientSideHash(BigInteger quantity) {
        final Function function = new Function(
                FUNC_ADDCLIENTSIDEHASH, 
                Arrays.<Type>asList(new Uint256(quantity)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> getLongTermHashRate() {
        final Function function = new Function(FUNC_GETLONGTERMHASHRATE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getShortTermHashRate() {
        final Function function = new Function(FUNC_GETSHORTTERMHASHRATE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> lastLongRestart() {
        final Function function = new Function(FUNC_LASTLONGRESTART, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> lastShortRestart() {
        final Function function = new Function(FUNC_LASTSHORTRESTART, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public static Hashrate load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Hashrate(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static Hashrate load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Hashrate(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class RestartLongEventResponse {
        public Log log;
    }

    public static class RestartShortEventResponse {
        public Log log;
    }

    public static class APIHashesEventResponse {
        public Log log;

        public BigInteger howMany;
    }

    public static class ClientSideHashEventResponse {
        public Log log;

        public String client;

        public BigInteger howMany;
    }

    public static class OwnershipRenouncedEventResponse {
        public Log log;

        public String previousOwner;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }
}
