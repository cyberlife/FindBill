package org.tensorflow.demo.ethereum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.4.0.
 */
public class FuPoW extends Contract {
    private static final String BINARY = "0x6060604052603c600655600060075560016008556014600955600060125560066013556000601460006101000a81548160ff0219169083151502179055506000601460016101000a81548160ff02191690831515021790555034156200006457600080fd5b6040516060806200226283398101604052808051906020019091908051906020019091908051906020019091905050600080336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600073ffffffffffffffffffffffffffffffffffffffff168573ffffffffffffffffffffffffffffffffffffffff16141515156200011357600080fd5b600115156200013686620002d26401000000000262001269176401000000009004565b15151415156200014557600080fd5b4283101515156200015557600080fd5b6001600654028301915060008084846200016e620002e5565b808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001848152602001838152602001828152602001945050505050604051809103906000f0801515620001d057600080fd5b905060058054806001018281620001e89190620002f6565b9160005260206000209001600083909190916101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505084600360006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555083600460006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555081600c8190555082600d8190555050505050506200034d565b600080823b905060008111915050919050565b60405161055e8062001d0483390190565b81548183558181151162000320578183600052602060002091820191016200031f919062000325565b5b505050565b6200034a91905b80821115620003465760008160009055506001016200032c565b5090565b90565b6119a7806200035d6000396000f30060606040526004361061011d576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680632a8e326514610122578063404ae8101461014b578063715018a61461017457806375f28e0a1461018957806378ff1748146101b25780637a5b9bd6146101db5780637c68f0b6146102045780638da5cb5b1461023d578063964edfd4146102925780639ade7f11146102fc578063abac66df14610351578063b07fae0f1461038a578063b36b0c28146103b7578063b4a433a5146103e0578063c51826e414610409578063d47f69a314610432578063dba12b4814610447578063e362650814610470578063e7bf12f91461049d578063f2fde38b146104c6578063f8fb1489146104ff575b600080fd5b341561012d57600080fd5b610135610528565b6040518082815260200191505060405180910390f35b341561015657600080fd5b61015e610532565b6040518082815260200191505060405180910390f35b341561017f57600080fd5b61018761053c565b005b341561019457600080fd5b61019c61063e565b6040518082815260200191505060405180910390f35b34156101bd57600080fd5b6101c5610648565b6040518082815260200191505060405180910390f35b34156101e657600080fd5b6101ee610658565b6040518082815260200191505060405180910390f35b341561020f57600080fd5b61023b600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610662565b005b341561024857600080fd5b610250610701565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561029d57600080fd5b6102fa600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803573ffffffffffffffffffffffffffffffffffffffff16906020019091908035906020019091908035906020019091905050610726565b005b341561030757600080fd5b61030f610b85565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561035c57600080fd5b610388600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610bcf565b005b341561039557600080fd5b61039d610da7565b604051808215151515815260200191505060405180910390f35b34156103c257600080fd5b6103ca610dc5565b6040518082815260200191505060405180910390f35b34156103eb57600080fd5b6103f3610dcf565b6040518082815260200191505060405180910390f35b341561041457600080fd5b61041c610dd9565b6040518082815260200191505060405180910390f35b341561043d57600080fd5b610445610de3565b005b341561045257600080fd5b61045a6111cd565b6040518082815260200191505060405180910390f35b341561047b57600080fd5b6104836111d7565b604051808215151515815260200191505060405180910390f35b34156104a857600080fd5b6104b06111ee565b6040518082815260200191505060405180910390f35b34156104d157600080fd5b6104fd600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506111f8565b005b341561050a57600080fd5b61051261125f565b6040518082815260200191505060405180910390f35b6000600c54905090565b6000600d54905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561059757600080fd5b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482060405160405180910390a260008060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550565b6000601154905090565b6000600160058054905003905090565b6000600e54905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156106bd57600080fd5b80600260006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b60008060011515600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151514806107d55750600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16145b15156107e057600080fd5b42600c54101515156107f157600080fd5b600015156107fe86611269565b151514151561080c57600080fd5b60328410151561081b57600080fd5b60001515601460009054906101000a900460ff16151514151561083d57600080fd5b603c420183111580156108535750603c42038310155b151561085e57600080fd5b600d5483036007819055506001601460006101000a81548160ff0219169083151502179055504360128190555082600b81905550600160058054905003600a819055506001600f5401600f819055506002600d54600c54038115156108bf57fe5b04600d5484031115156108d9576001601054016010819055505b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1691508173ffffffffffffffffffffffffffffffffffffffff1663c74a073e878787886064036040518563ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001838152602001828152602001945050505050600060405180830381600087803b15156109df57600080fd5b6102c65a03f115156109f057600080fd5b5050506005600160058054905003815481101515610a0a57fe5b906000526020600020900160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508073ffffffffffffffffffffffffffffffffffffffff1663d9fca769866040518263ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001915050600060405180830381600087803b1515610ad257600080fd5b6102c65a03f11515610ae357600080fd5b5050507f6bbee3eb9a1e70d55e7199f9956b9f9585d3b58693ee76af58f7b309f231eb4f8686604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060405180910390a1505050505050565b60006005600160058054905003815481101515610b9e57fe5b906000526020600020900160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610c2a57600080fd5b60011515610c3782611269565b1515141515610c4557600080fd5b600160008273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615600160008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055507f81e550bfe75194d94a4fe86d981d6157de7fa41fa48078d14294f32868e27b7e81600160008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001821515151581526020019250505060405180910390a150565b6000600c5442101515610dbd5760019050610dc2565b600090505b90565b6000601354905090565b6000600654905090565b6000601054905090565b60008060008060011515601460009054906101000a900460ff1615151480610e0d5750600c544210155b1515610e1857600080fd5b60001515601460019054906101000a900460ff161515141515610e3a57600080fd5b6001601460016101000a81548160ff021916908315150217905550601354600e54600580549050031415610f2a576002601354811515610e7657fe5b0460105410158015610e9857506002601354811515610e9157fe5b04600f5410155b15610ed9576003601054111515610eba57600160085401600881905550610ed4565b6002601054811515610ec857fe5b04600854016008819055505b610f0d565b6002601354811515610ee757fe5b04600f54108015610efa57506001600854115b15610f0c576001600854036008819055505b5b600580549050600e819055506000600f8190555060006010819055505b4292506005600160058054905003815481101515610f4457fe5b906000526020600020900160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16600580549050846001600654028601610f876113bc565b808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001848152602001838152602001828152602001945050505050604051809103906000f0801515610fe857600080fd5b935060058054806001018281610ffe91906113cc565b9160005260206000209001600086909190916101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505060006012819055506001600654028301600c8190555082600d819055506000601460006101000a81548160ff02191690831515021790555061108e4361127c565b9150600082111561117557600460009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508073ffffffffffffffffffffffffffffffffffffffff16632534522333846040518363ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050600060405180830381600087803b151561116057600080fd5b6102c65a03f1151561117157600080fd5b5050505b6000601460016101000a81548160ff0219169083151502179055507fa4ecee8910d246b2ad843067bd50d0881d262426e59e96b82017758542f6dd39836040518082815260200191505060405180910390a150505050565b6000600854905090565b6000601460009054906101000a900460ff16905090565b6000600a54905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561125357600080fd5b61125c816112c2565b50565b6000600b54905090565b600080823b905060008111915050919050565b600060036012548303111561129457600090506112bd565b6012548214156112a85760095490506112bd565b60125482036009548115156112b957fe5b0490505b919050565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff16141515156112fe57600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a3806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b60405161055e8061141e83390190565b8154818355818115116113f3578183600052602060002091820191016113f291906113f8565b5b505050565b61141a91905b808211156114165760008160009055506001016113fe565b5090565b9056006060604052341561000f57600080fd5b60405160808061055e83398101604052808051906020019091908051906020019091908051906020019091908051906020019091905050336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060a0604051908101604052808573ffffffffffffffffffffffffffffffffffffffff168152602001600073ffffffffffffffffffffffffffffffffffffffff16815260200184815260200183815260200182815250600160008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060208201518160010160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550604082015181600201556060820151816003015560808201518160040155905050505050506103bf8061019f6000396000f300606060405260043610610078576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680631f89f25e1461007d57806374e94deb146100a657806381045ead146100cf57806384345424146100f85780638e7ea5b21461014d578063d9fca769146101a2575b600080fd5b341561008857600080fd5b6100906101db565b6040518082815260200191505060405180910390f35b34156100b157600080fd5b6100b96101e8565b6040518082815260200191505060405180910390f35b34156100da57600080fd5b6100e26101f5565b6040518082815260200191505060405180910390f35b341561010357600080fd5b61010b610202565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561015857600080fd5b61016061022f565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34156101ad57600080fd5b6101d9600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190505061025b565b005b6000600160030154905090565b6000600160040154905090565b6000600160020154905090565b6000600160000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b60006001800160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b3373ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff161415156102b657600080fd5b806001800160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507ff81769dfe0bee7eefda9487747d034608189c3eaad042d3629ed261063595cad3382604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060405180910390a1505600a165627a7a72305820920115187aba3346530c6f5506d8d9ad7b88bedbb9c0e821984c41984e209c190029a165627a7a72305820d45b2c01f5415ef45fca244d5b01705ad211c6f27f7420637083c839e68002d200296060604052341561000f57600080fd5b60405160808061055e83398101604052808051906020019091908051906020019091908051906020019091908051906020019091905050336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060a0604051908101604052808573ffffffffffffffffffffffffffffffffffffffff168152602001600073ffffffffffffffffffffffffffffffffffffffff16815260200184815260200183815260200182815250600160008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060208201518160010160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550604082015181600201556060820151816003015560808201518160040155905050505050506103bf8061019f6000396000f300606060405260043610610078576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680631f89f25e1461007d57806374e94deb146100a657806381045ead146100cf57806384345424146100f85780638e7ea5b21461014d578063d9fca769146101a2575b600080fd5b341561008857600080fd5b6100906101db565b6040518082815260200191505060405180910390f35b34156100b157600080fd5b6100b96101e8565b6040518082815260200191505060405180910390f35b34156100da57600080fd5b6100e26101f5565b6040518082815260200191505060405180910390f35b341561010357600080fd5b61010b610202565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561015857600080fd5b61016061022f565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34156101ad57600080fd5b6101d9600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190505061025b565b005b6000600160030154905090565b6000600160040154905090565b6000600160020154905090565b6000600160000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b60006001800160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b3373ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff161415156102b657600080fd5b806001800160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507ff81769dfe0bee7eefda9487747d034608189c3eaad042d3629ed261063595cad3382604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060405180910390a1505600a165627a7a72305820920115187aba3346530c6f5506d8d9ad7b88bedbb9c0e821984c41984e209c190029";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final String FUNC_TOGGLECLIENTSIDEPROCESSING = "toggleClientSideProcessing";

    public static final String FUNC_CREATENEWLEVEL = "createNewLevel";

    public static final String FUNC_FOUNDSOLUTION = "foundSolution";

    public static final String FUNC_ADDOPI = "addOPI";

    public static final String FUNC_GETLEVELNUMBER = "getLevelNumber";

    public static final String FUNC_GETLEVELDURATION = "getLevelDuration";

    public static final String FUNC_GETPOSITIONLASTFIND = "getPositionLastFind";

    public static final String FUNC_GETFOUNDEARLIER = "getFoundEarlier";

    public static final String FUNC_GETTIMESTAMPLASTFIND = "getTimestampLastFind";

    public static final String FUNC_GETOBJECTSTOFIND = "getObjectsToFind";

    public static final String FUNC_GETCURRENTROUNDDEADLINE = "getCurrentRoundDeadline";

    public static final String FUNC_GETCURRENTLEVELADDRESS = "getCurrentLevelAddress";

    public static final String FUNC_GETCURRENTROUNDSTART = "getCurrentRoundStart";

    public static final String FUNC_GETLASTADJUSTMENTPOSITION = "getLastAdjustmentPosition";

    public static final String FUNC_GETDIFFICULTYADJUSTMENTINTERVAL = "getDifficultyAdjustmentInterval";

    public static final String FUNC_GETFOUNDCURRENTANSWER = "getFoundCurrentAnswer";

    public static final String FUNC_GETPLAYERSONLINE = "getPlayersOnline";

    public static final String FUNC_LEVELTERMINATED = "levelTerminated";

    public static final Event FOUNDSOLUTION_EVENT = new Event("FoundSolution", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event CLIENTSIDEHASHED_EVENT = new Event("ClientSideHashed", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event CREATEDNEWLEVEL_EVENT = new Event("CreatedNewLevel", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
    ;

    public static final Event TOGGLEDCLIENT_EVENT = new Event("ToggledClient", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Bool>() {}));
    ;

    public static final Event OWNERSHIPRENOUNCED_EVENT = new Event("OwnershipRenounced", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
    }

    protected FuPoW(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected FuPoW(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String _newOwner) {
        final Function function = new Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new Address(_newOwner)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<FuPoW> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String rareTokenAddr, String commonTokenAddr, BigInteger timestampStartGame) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Address(rareTokenAddr),
                new Address(commonTokenAddr),
                new Uint256(timestampStartGame)));
        return deployRemoteCall(FuPoW.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static RemoteCall<FuPoW> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String rareTokenAddr, String commonTokenAddr, BigInteger timestampStartGame) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Address(rareTokenAddr),
                new Address(commonTokenAddr),
                new Uint256(timestampStartGame)));
        return deployRemoteCall(FuPoW.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public List<FoundSolutionEventResponse> getFoundSolutionEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(FOUNDSOLUTION_EVENT, transactionReceipt);
        ArrayList<FoundSolutionEventResponse> responses = new ArrayList<FoundSolutionEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            FoundSolutionEventResponse typedResponse = new FoundSolutionEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.platform = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.who = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<FoundSolutionEventResponse> foundSolutionEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, FoundSolutionEventResponse>() {
            @Override
            public FoundSolutionEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(FOUNDSOLUTION_EVENT, log);
                FoundSolutionEventResponse typedResponse = new FoundSolutionEventResponse();
                typedResponse.log = log;
                typedResponse.platform = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.who = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<FoundSolutionEventResponse> foundSolutionEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(FOUNDSOLUTION_EVENT));
        return foundSolutionEventObservable(filter);
    }

    public List<ClientSideHashedEventResponse> getClientSideHashedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(CLIENTSIDEHASHED_EVENT, transactionReceipt);
        ArrayList<ClientSideHashedEventResponse> responses = new ArrayList<ClientSideHashedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            ClientSideHashedEventResponse typedResponse = new ClientSideHashedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.success = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.client = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ClientSideHashedEventResponse> clientSideHashedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, ClientSideHashedEventResponse>() {
            @Override
            public ClientSideHashedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(CLIENTSIDEHASHED_EVENT, log);
                ClientSideHashedEventResponse typedResponse = new ClientSideHashedEventResponse();
                typedResponse.log = log;
                typedResponse.success = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.client = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<ClientSideHashedEventResponse> clientSideHashedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(CLIENTSIDEHASHED_EVENT));
        return clientSideHashedEventObservable(filter);
    }

    public List<CreatedNewLevelEventResponse> getCreatedNewLevelEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(CREATEDNEWLEVEL_EVENT, transactionReceipt);
        ArrayList<CreatedNewLevelEventResponse> responses = new ArrayList<CreatedNewLevelEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            CreatedNewLevelEventResponse typedResponse = new CreatedNewLevelEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<CreatedNewLevelEventResponse> createdNewLevelEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, CreatedNewLevelEventResponse>() {
            @Override
            public CreatedNewLevelEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(CREATEDNEWLEVEL_EVENT, log);
                CreatedNewLevelEventResponse typedResponse = new CreatedNewLevelEventResponse();
                typedResponse.log = log;
                typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<CreatedNewLevelEventResponse> createdNewLevelEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(CREATEDNEWLEVEL_EVENT));
        return createdNewLevelEventObservable(filter);
    }

    public List<ToggledClientEventResponse> getToggledClientEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(TOGGLEDCLIENT_EVENT, transactionReceipt);
        ArrayList<ToggledClientEventResponse> responses = new ArrayList<ToggledClientEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            ToggledClientEventResponse typedResponse = new ToggledClientEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.client = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.state = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ToggledClientEventResponse> toggledClientEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, ToggledClientEventResponse>() {
            @Override
            public ToggledClientEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(TOGGLEDCLIENT_EVENT, log);
                ToggledClientEventResponse typedResponse = new ToggledClientEventResponse();
                typedResponse.log = log;
                typedResponse.client = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.state = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<ToggledClientEventResponse> toggledClientEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(TOGGLEDCLIENT_EVENT));
        return toggledClientEventObservable(filter);
    }

    public List<OwnershipRenouncedEventResponse> getOwnershipRenouncedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, transactionReceipt);
        ArrayList<OwnershipRenouncedEventResponse> responses = new ArrayList<OwnershipRenouncedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipRenouncedEventResponse>() {
            @Override
            public OwnershipRenouncedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, log);
                OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPRENOUNCED_EVENT));
        return ownershipRenouncedEventObservable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventObservable(filter);
    }

    public RemoteCall<TransactionReceipt> toggleClientSideProcessing(String clientSide) {
        final Function function = new Function(
                FUNC_TOGGLECLIENTSIDEPROCESSING, 
                Arrays.<Type>asList(new Address(clientSide)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> createNewLevel() {
        final Function function = new Function(
                FUNC_CREATENEWLEVEL, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> foundSolution(String platform, String who, BigInteger platformShare, BigInteger timestamp) {
        final Function function = new Function(
                FUNC_FOUNDSOLUTION, 
                Arrays.<Type>asList(new Address(platform),
                new Address(who),
                new Uint256(platformShare),
                new Uint256(timestamp)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> addOPI(String _opi) {
        final Function function = new Function(
                FUNC_ADDOPI, 
                Arrays.<Type>asList(new Address(_opi)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> getLevelNumber() {
        final Function function = new Function(FUNC_GETLEVELNUMBER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getLevelDuration() {
        final Function function = new Function(FUNC_GETLEVELDURATION, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getPositionLastFind() {
        final Function function = new Function(FUNC_GETPOSITIONLASTFIND, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getFoundEarlier() {
        final Function function = new Function(FUNC_GETFOUNDEARLIER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getTimestampLastFind() {
        final Function function = new Function(FUNC_GETTIMESTAMPLASTFIND, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getObjectsToFind() {
        final Function function = new Function(FUNC_GETOBJECTSTOFIND, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getCurrentRoundDeadline() {
        final Function function = new Function(FUNC_GETCURRENTROUNDDEADLINE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<String> getCurrentLevelAddress() {
        final Function function = new Function(FUNC_GETCURRENTLEVELADDRESS, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<BigInteger> getCurrentRoundStart() {
        final Function function = new Function(FUNC_GETCURRENTROUNDSTART, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getLastAdjustmentPosition() {
        final Function function = new Function(FUNC_GETLASTADJUSTMENTPOSITION, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getDifficultyAdjustmentInterval() {
        final Function function = new Function(FUNC_GETDIFFICULTYADJUSTMENTINTERVAL, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Boolean> getFoundCurrentAnswer() {
        final Function function = new Function(FUNC_GETFOUNDCURRENTANSWER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<BigInteger> getPlayersOnline() {
        final Function function = new Function(FUNC_GETPLAYERSONLINE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Boolean> levelTerminated() {
        final Function function = new Function(FUNC_LEVELTERMINATED, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public static FuPoW load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new FuPoW(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static FuPoW load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new FuPoW(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class FoundSolutionEventResponse {
        public Log log;

        public String platform;

        public String who;
    }

    public static class ClientSideHashedEventResponse {
        public Log log;

        public Boolean success;

        public String client;
    }

    public static class CreatedNewLevelEventResponse {
        public Log log;

        public BigInteger timestamp;
    }

    public static class ToggledClientEventResponse {
        public Log log;

        public String client;

        public Boolean state;
    }

    public static class OwnershipRenouncedEventResponse {
        public Log log;

        public String previousOwner;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }
}
