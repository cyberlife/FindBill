package org.tensorflow.demo.ethereum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.4.0.
 */
public class Sessions extends Contract {
    private static final String BINARY = "0x6060604052341561000f57600080fd5b336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550612b048061005e6000396000f3006060604052600436106100e6576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680630b5cca9e146100eb57806337eed1e514610165578063505799f4146101df578063557ed1ba146102675780635bb877441461029057806363023bea146102f65780636f7e30341461039c578063715018a6146104455780638da5cb5b1461045a578063b2b5c052146104af578063b6ce2b6814610534578063e16b4823146105fc578063e369237514610676578063e38cf509146106f4578063eec9c3c8146107d3578063f2fde38b1461084d575b600080fd5b34156100f657600080fd5b61014f600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050610886565b6040518082815260200191505060405180910390f35b341561017057600080fd5b6101c9600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803590602001909190505061091a565b6040518082815260200191505060405180910390f35b610265600480803590602001909190803590602001909190803590602001909190803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091908035906020019091908035906020019091905050610939565b005b341561027257600080fd5b61027a610d17565b6040518082815260200191505060405180910390f35b341561029b57600080fd5b6102f4600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050610d1f565b005b341561030157600080fd5b61035a600480803590602001908201803590602001908080601f016020809104026020016040519081016040528093929190818152602001838380828437820191505050505050919080359060200190919050506110c9565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34156103a757600080fd5b610443600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803590602001909190803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190505061117d565b005b341561045057600080fd5b61045861146b565b005b341561046557600080fd5b61046d61156d565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34156104ba57600080fd5b610532600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803590602001909190803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050611592565b005b341561053f57600080fd5b6105fa600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803590602001909190803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050611899565b005b341561060757600080fd5b610660600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050611a8e565b6040518082815260200191505060405180910390f35b341561068157600080fd5b6106da600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050611b22565b604051808215151515815260200191505060405180910390f35b34156106ff57600080fd5b610758600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050611bc3565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561079857808201518184015260208101905061077d565b50505050905090810190601f1680156107c55780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156107de57600080fd5b610837600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050611cf5565b6040518082815260200191505060405180910390f35b341561085857600080fd5b610884600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050611d89565b005b60006001836040518082805190602001908083835b6020831015156108c0578051825260208201915060208101905060208303925061089b565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208281548110151561090057fe5b90600052602060002090600a020160080154905092915050565b60006109268383611cf5565b6109308484611a8e565b03905092915050565b6000610943611eea565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561099e57600080fd5b42891115156109ac57600080fd5b603c88101515156109bc57600080fd5b600187101515156109cc57600080fd5b600086511115156109dc57600080fd5b6000341115156109eb57600080fd5b6109f3611f65565b604051809103906000f0801515610a0957600080fd5b9150610160604051908101604052806000151581526020016040805190810160405280600681526020017f4e6f626f6479000000000000000000000000000000000000000000000000000081525081526020018a815260200160018a028b0181526020018881526020018681526020018581526020018481526020013481526020016000151581526020018373ffffffffffffffffffffffffffffffffffffffff1681525090506001866040518082805190602001908083835b602083101515610ae85780518252602082019150602081019050602083039250610ac3565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208054806001018281610b2d9190611f75565b91600052602060002090600a0201600083909190915060008201518160000160006101000a81548160ff0219169083151502179055506020820151816001019080519060200190610b7f929190611fa7565b5060408201518160020155606082015181600301556080820151816004015560a0820151816005015560c0820151816006015560e0820151816007015561010082015181600801556101208201518160090160006101000a81548160ff0219169083151502179055506101408201518160090160016101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505050507fdcebafeae8c357cebefd710f2f0d1407d15d6b2ec501c80451b76b5e606a96d38960018a028b018989898989346040518089815260200188815260200187815260200180602001868152602001858152602001848152602001838152602001828103825287818151815260200191508051906020019080838360005b83811015610ccb578082015181840152602081019050610cb0565b50505050905090810190601f168015610cf85780820380516001836020036101000a031916815260200191505b50995050505050505050505060405180910390a1505050505050505050565b600042905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610d7a57600080fd5b60008251111515610d8a57600080fd5b600015156001836040518082805190602001908083835b602083101515610dc65780518252602082019150602081019050602083039250610da1565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902082815481101515610e0657fe5b90600052602060002090600a020160000160009054906101000a900460ff161515141515610e3357600080fd5b42620151806001846040518082805190602001908083835b602083101515610e705780518252602082019150602081019050602083039250610e4b565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902083815481101515610eb057fe5b90600052602060002090600a02016003015401101515610ecf57600080fd5b3373ffffffffffffffffffffffffffffffffffffffff166108fc6001846040518082805190602001908083835b602083101515610f215780518252602082019150602081019050602083039250610efc565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902083815481101515610f6157fe5b90600052602060002090600a0201600801549081150290604051600060405180830381858888f193505050501515610f9857600080fd5b7f334303de8da6019ceb3a6552edafcd6b3707e9c1e59d2a725570c605d9e005036001836040518082805190602001908083835b602083101515610ff15780518252602082019150602081019050602083039250610fcc565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208281548110151561103157fe5b90600052602060002090600a020160020154836040518083815260200180602001828103825283818151815260200191508051906020019080838360005b8381101561108a57808201518184015260208101905061106f565b50505050905090810190601f1680156110b75780820380516001836020036101000a031916815260200191505b50935050505060405180910390a15050565b60006001836040518082805190602001908083835b60208310151561110357805182526020820191506020810190506020830392506110de565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208281548110151561114357fe5b90600052602060002090600a020160090160019054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905092915050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156111d857600080fd5b600081511115156111e857600080fd5b600083511115156111f857600080fd5b426001846040518082805190602001908083835b602083101515611231578051825260208201915060208101905060208303925061120c565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208381548110151561127157fe5b90600052602060002090600a02016003015411151561128f57600080fd5b6001836040518082805190602001908083835b6020831015156112c757805182526020820191506020810190506020830392506112a2565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208281548110151561130757fe5b90600052602060002090600a020160020154421015151561132757600080fd5b600180846040518082805190602001908083835b602083101515611360578051825260208201915060208101905060208303925061133b565b6001836020036101000a0380198251168184511680821785525050505050509050019150509081526020016040518091039020838154811015156113a057fe5b90600052602060002090600a020160000160006101000a81548160ff021916908315150217905550806001846040518082805190602001908083835b60208310151561140157805182526020820191506020810190506020830392506113dc565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208381548110151561144157fe5b90600052602060002090600a02016001019080519060200190611465929190612027565b50505050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156114c657600080fd5b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482060405160405180910390a260008060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156115ed57600080fd5b600083511115156115fd57600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166108fc6001856040518082805190602001908083835b60208310151561164f578051825260208201915060208101905060208303925061162a565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208481548110151561168f57fe5b90600052602060002090600a0201600801549081150290604051600060405180830381858888f1935050505015156116c657600080fd5b600180846040518082805190602001908083835b6020831015156116ff57805182526020820191506020810190506020830392506116da565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208381548110151561173f57fe5b90600052602060002090600a020160090160006101000a81548160ff0219169083151502179055507ff88874e1cfd5ab765d3d4660b09e96dfee9c0ab072b29719ac313757eb7fb17f836001856040518082805190602001908083835b6020831015156117c1578051825260208201915060208101905060208303925061179c565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208481548110151561180157fe5b90600052602060002090600a0201600801546040518080602001838152602001828103825284818151815260200191508051906020019080838360005b8381101561185957808201518184015260208101905061183e565b50505050905090810190601f1680156118865780820380516001836020036101000a031916815260200191505b50935050505060405180910390a1505050565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156118f657600080fd5b6000855111151561190657600080fd5b6001856040518082805190602001908083835b60208310151561193e5780518252602082019150602081019050602083039250611919565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208481548110151561197e57fe5b90600052602060002090600a0201600201544210151561199d57600080fd5b8290508073ffffffffffffffffffffffffffffffffffffffff166327e9f294836040518263ffffffff167c01000000000000000000000000000000000000000000000000000000000281526004018080602001828103825283818151815260200191508051906020019080838360005b83811015611a28578082015181840152602081019050611a0d565b50505050905090810190601f168015611a555780820380516001836020036101000a031916815260200191505b5092505050600060405180830381600087803b1515611a7357600080fd5b6102c65a03f11515611a8457600080fd5b5050505050505050565b60006001836040518082805190602001908083835b602083101515611ac85780518252602082019150602081019050602083039250611aa3565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902082815481101515611b0857fe5b90600052602060002090600a020160030154905092915050565b60006001836040518082805190602001908083835b602083101515611b5c5780518252602082019150602081019050602083039250611b37565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902082815481101515611b9c57fe5b90600052602060002090600a020160090160009054906101000a900460ff16905092915050565b611bcb6120a7565b6001836040518082805190602001908083835b602083101515611c035780518252602082019150602081019050602083039250611bde565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902082815481101515611c4357fe5b90600052602060002090600a02016001018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611ce85780601f10611cbd57610100808354040283529160200191611ce8565b820191906000526020600020905b815481529060010190602001808311611ccb57829003601f168201915b5050505050905092915050565b60006001836040518082805190602001908083835b602083101515611d2f5780518252602082019150602081019050602083039250611d0a565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902082815481101515611d6f57fe5b90600052602060002090600a020160020154905092915050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515611de457600080fd5b611ded81611df0565b50565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff1614151515611e2c57600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a3806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b61016060405190810160405280600015158152602001611f086120bb565b815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600015158152602001600073ffffffffffffffffffffffffffffffffffffffff1681525090565b6040516108e6806121f383390190565b815481835581811511611fa257600a0281600a028360005260206000209182019101611fa191906120cf565b5b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10611fe857805160ff1916838001178555612016565b82800160010185558215612016579182015b82811115612015578251825591602001919060010190611ffa565b5b5090506120239190612185565b5090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061206857805160ff1916838001178555612096565b82800160010185558215612096579182015b8281111561209557825182559160200191906001019061207a565b5b5090506120a39190612185565b5090565b602060405190810160405280600081525090565b602060405190810160405280600081525090565b61218291905b8082111561217e57600080820160006101000a81549060ff021916905560018201600061210291906121aa565b60028201600090556003820160009055600482016000905560058201600090556006820160009055600782016000905560088201600090556009820160006101000a81549060ff02191690556009820160016101000a81549073ffffffffffffffffffffffffffffffffffffffff021916905550600a016120d5565b5090565b90565b6121a791905b808211156121a357600081600090555060010161218b565b5090565b90565b50805460018160011615610100020316600290046000825580601f106121d057506121ef565b601f0160209004906000526020600020908101906121ee9190612185565b5b5056006060604052341561000f57600080fd5b336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506108888061005e6000396000f300606060405260043610610078576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806327e9f2941461007d5780633129e773146100da578063715018a61461017657806377a057421461018b5780638da5cb5b146101b4578063f2fde38b14610209575b600080fd5b341561008857600080fd5b6100d8600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050610242565b005b34156100e557600080fd5b6100fb60048080359060200190919050506103ac565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561013b578082015181840152602081019050610120565b50505050905090810190601f1680156101685780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561018157600080fd5b61018961046e565b005b341561019657600080fd5b61019e610570565b6040518082815260200191505060405180910390f35b34156101bf57600080fd5b6101c761057d565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561021457600080fd5b610240600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506105a2565b005b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561029d57600080fd5b600180548060010182816102b19190610703565b9160005260206000209001600083909190915090805190602001906102d792919061072f565b50507feb5bbb9747321095b994890b33bee1d5a981d97286012f33cdfc440a31806f68813360405180806020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001828103825284818151815260200191508051906020019080838360005b8381101561036e578082015181840152602081019050610353565b50505050905090810190601f16801561039b5780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150565b6103b46107af565b6001828154811015156103c357fe5b90600052602060002090018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156104625780601f1061043757610100808354040283529160200191610462565b820191906000526020600020905b81548152906001019060200180831161044557829003601f168201915b50505050509050919050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156104c957600080fd5b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482060405160405180910390a260008060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550565b6000600180549050905090565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156105fd57600080fd5b61060681610609565b50565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff161415151561064557600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a3806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b81548183558181151161072a5781836000526020600020918201910161072991906107c3565b5b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061077057805160ff191683800117855561079e565b8280016001018555821561079e579182015b8281111561079d578251825591602001919060010190610782565b5b5090506107ab91906107ef565b5090565b602060405190810160405280600081525090565b6107ec91905b808211156107e857600081816107df9190610814565b506001016107c9565b5090565b90565b61081191905b8082111561080d5760008160009055506001016107f5565b5090565b90565b50805460018160011615610100020316600290046000825580601f1061083a5750610859565b601f01602090049060005260206000209081019061085891906107ef565b5b505600a165627a7a72305820c13efa10bcc0bb35737e6c2915bb1c5d9ca2e79b776aea3f3f3647303e87a0390029a165627a7a723058200422dd454b1917e079d138567305668a8ecbfa13937fc0fbf35395d6529e2d7d0029";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final String FUNC_NEWSESSION = "newSession";

    public static final String FUNC_ADDITEM = "addItem";

    public static final String FUNC_FOUNDTHERESPONSE = "foundTheResponse";

    public static final String FUNC_CLAIMPRIZE = "claimPrize";

    public static final String FUNC_NOBODYWONSOWITHDRAW = "nobodyWonSoWithdraw";

    public static final String FUNC_GETSTARTTIME = "getStartTime";

    public static final String FUNC_GETENDTIME = "getEndTime";

    public static final String FUNC_GETPRIZE = "getPrize";

    public static final String FUNC_GETWINNER = "getWinner";

    public static final String FUNC_PRIZEALREADYTAKEN = "prizeAlreadyTaken";

    public static final String FUNC_GETITEMSLOCATION = "getItemsLocation";

    public static final String FUNC_GETDURATION = "getDuration";

    public static final String FUNC_GETTIME = "getTime";

    public static final Event NEWSESSION_EVENT = new Event("NewSession", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event SOMEONEWON_EVENT = new Event("SomeoneWon", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event NOBODYWONANDWITHDREW_EVENT = new Event("NobodyWonAndWithdrew", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Utf8String>() {}));
    ;

    public static final Event OWNERSHIPRENOUNCED_EVENT = new Event("OwnershipRenounced", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
    }

    protected Sessions(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Sessions(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String _newOwner) {
        final Function function = new Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new Address(_newOwner)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<Sessions> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Sessions.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Sessions> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Sessions.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public List<NewSessionEventResponse> getNewSessionEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(NEWSESSION_EVENT, transactionReceipt);
        ArrayList<NewSessionEventResponse> responses = new ArrayList<NewSessionEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            NewSessionEventResponse typedResponse = new NewSessionEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.start = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.end = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.subsessions = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            typedResponse.name = (String) eventValues.getNonIndexedValues().get(3).getValue();
            typedResponse.latitude = (BigInteger) eventValues.getNonIndexedValues().get(4).getValue();
            typedResponse.longitude = (BigInteger) eventValues.getNonIndexedValues().get(5).getValue();
            typedResponse.radius = (BigInteger) eventValues.getNonIndexedValues().get(6).getValue();
            typedResponse.prize = (BigInteger) eventValues.getNonIndexedValues().get(7).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<NewSessionEventResponse> newSessionEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, NewSessionEventResponse>() {
            @Override
            public NewSessionEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(NEWSESSION_EVENT, log);
                NewSessionEventResponse typedResponse = new NewSessionEventResponse();
                typedResponse.log = log;
                typedResponse.start = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.end = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.subsessions = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                typedResponse.name = (String) eventValues.getNonIndexedValues().get(3).getValue();
                typedResponse.latitude = (BigInteger) eventValues.getNonIndexedValues().get(4).getValue();
                typedResponse.longitude = (BigInteger) eventValues.getNonIndexedValues().get(5).getValue();
                typedResponse.radius = (BigInteger) eventValues.getNonIndexedValues().get(6).getValue();
                typedResponse.prize = (BigInteger) eventValues.getNonIndexedValues().get(7).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<NewSessionEventResponse> newSessionEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEWSESSION_EVENT));
        return newSessionEventObservable(filter);
    }

    public List<SomeoneWonEventResponse> getSomeoneWonEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(SOMEONEWON_EVENT, transactionReceipt);
        ArrayList<SomeoneWonEventResponse> responses = new ArrayList<SomeoneWonEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            SomeoneWonEventResponse typedResponse = new SomeoneWonEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._name = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.prize = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<SomeoneWonEventResponse> someoneWonEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, SomeoneWonEventResponse>() {
            @Override
            public SomeoneWonEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(SOMEONEWON_EVENT, log);
                SomeoneWonEventResponse typedResponse = new SomeoneWonEventResponse();
                typedResponse.log = log;
                typedResponse._name = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.prize = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<SomeoneWonEventResponse> someoneWonEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(SOMEONEWON_EVENT));
        return someoneWonEventObservable(filter);
    }

    public List<NobodyWonAndWithdrewEventResponse> getNobodyWonAndWithdrewEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(NOBODYWONANDWITHDREW_EVENT, transactionReceipt);
        ArrayList<NobodyWonAndWithdrewEventResponse> responses = new ArrayList<NobodyWonAndWithdrewEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            NobodyWonAndWithdrewEventResponse typedResponse = new NobodyWonAndWithdrewEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.startTime = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse._name = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<NobodyWonAndWithdrewEventResponse> nobodyWonAndWithdrewEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, NobodyWonAndWithdrewEventResponse>() {
            @Override
            public NobodyWonAndWithdrewEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(NOBODYWONANDWITHDREW_EVENT, log);
                NobodyWonAndWithdrewEventResponse typedResponse = new NobodyWonAndWithdrewEventResponse();
                typedResponse.log = log;
                typedResponse.startTime = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse._name = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<NobodyWonAndWithdrewEventResponse> nobodyWonAndWithdrewEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NOBODYWONANDWITHDREW_EVENT));
        return nobodyWonAndWithdrewEventObservable(filter);
    }

    public List<OwnershipRenouncedEventResponse> getOwnershipRenouncedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, transactionReceipt);
        ArrayList<OwnershipRenouncedEventResponse> responses = new ArrayList<OwnershipRenouncedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipRenouncedEventResponse>() {
            @Override
            public OwnershipRenouncedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, log);
                OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPRENOUNCED_EVENT));
        return ownershipRenouncedEventObservable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventObservable(filter);
    }

    public RemoteCall<TransactionReceipt> newSession(BigInteger start, BigInteger duration, BigInteger subsessions, String name, BigInteger latitude, BigInteger longitude, BigInteger radius, BigInteger weiValue) {
        final Function function = new Function(
                FUNC_NEWSESSION, 
                Arrays.<Type>asList(new Uint256(start),
                new Uint256(duration),
                new Uint256(subsessions),
                new Utf8String(name),
                new Uint256(latitude),
                new Uint256(longitude),
                new Uint256(radius)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteCall<TransactionReceipt> addItem(String _name, BigInteger position, String itemsAddr, String itemHash) {
        final Function function = new Function(
                FUNC_ADDITEM, 
                Arrays.<Type>asList(new Utf8String(_name),
                new Uint256(position),
                new Address(itemsAddr),
                new Utf8String(itemHash)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> foundTheResponse(String _name, BigInteger position, String who) {
        final Function function = new Function(
                FUNC_FOUNDTHERESPONSE, 
                Arrays.<Type>asList(new Utf8String(_name),
                new Uint256(position),
                new Utf8String(who)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> claimPrize(String _name, BigInteger position, String _who) {
        final Function function = new Function(
                FUNC_CLAIMPRIZE, 
                Arrays.<Type>asList(new Utf8String(_name),
                new Uint256(position),
                new Address(_who)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> nobodyWonSoWithdraw(String _name, BigInteger position) {
        final Function function = new Function(
                FUNC_NOBODYWONSOWITHDRAW, 
                Arrays.<Type>asList(new Utf8String(_name),
                new Uint256(position)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> getStartTime(String name, BigInteger position) {
        final Function function = new Function(FUNC_GETSTARTTIME, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getEndTime(String name, BigInteger position) {
        final Function function = new Function(FUNC_GETENDTIME, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getPrize(String name, BigInteger position) {
        final Function function = new Function(FUNC_GETPRIZE, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<String> getWinner(String name, BigInteger position) {
        final Function function = new Function(FUNC_GETWINNER, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<Boolean> prizeAlreadyTaken(String name, BigInteger position) {
        final Function function = new Function(FUNC_PRIZEALREADYTAKEN, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<String> getItemsLocation(String name, BigInteger position) {
        final Function function = new Function(FUNC_GETITEMSLOCATION, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<BigInteger> getDuration(String name, BigInteger position) {
        final Function function = new Function(FUNC_GETDURATION, 
                Arrays.<Type>asList(new Utf8String(name),
                new Uint256(position)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> getTime() {
        final Function function = new Function(FUNC_GETTIME, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public static Sessions load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Sessions(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static Sessions load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Sessions(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class NewSessionEventResponse {
        public Log log;

        public BigInteger start;

        public BigInteger end;

        public BigInteger subsessions;

        public String name;

        public BigInteger latitude;

        public BigInteger longitude;

        public BigInteger radius;

        public BigInteger prize;
    }

    public static class SomeoneWonEventResponse {
        public Log log;

        public String _name;

        public BigInteger prize;
    }

    public static class NobodyWonAndWithdrewEventResponse {
        public Log log;

        public BigInteger startTime;

        public String _name;
    }

    public static class OwnershipRenouncedEventResponse {
        public Log log;

        public String previousOwner;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }
}
