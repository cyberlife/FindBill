package org.tensorflow.demo.ethereum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.4.0.
 */
public class FuOPI extends Contract {
    private static final String BINARY = "0x60606040526101f4600b55610708600c55341561001b57600080fd5b6040516080806126c283398101604052808051906020019091908051906020019091908051906020019091908051906020019091905050336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555083600660006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555082600760006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555081600960006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555080600a60006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050505050612519806101a96000396000f3006060604052600436106100d0576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806340f5d6a6146100d55780636a326ab11461017e578063715018a6146101b75780638da5cb5b146101cc578063a911ceb014610221578063aecaf5441461025a578063b9c0433a14610293578063c59e65da146102b6578063cb2b5e12146102ef578063d4c143fd14610328578063d4c988d71461034b578063dbd3cd62146103c3578063e359c68b146103e6578063f2fde38b14610455575b600080fd5b34156100e057600080fd5b61017c600480803590602001909190803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803590602001909190803590602001909190803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803590602001909190803590602001909190505061048e565b005b341561018957600080fd5b6101b5600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610c1e565b005b34156101c257600080fd5b6101ca610cbd565b005b34156101d757600080fd5b6101df610dbf565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561022c57600080fd5b610258600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610de4565b005b341561026557600080fd5b610291600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610e83565b005b341561029e57600080fd5b6102b4600480803590602001909190505061105e565b005b34156102c157600080fd5b6102ed600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190505061121b565b005b34156102fa57600080fd5b610326600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506112ba565b005b341561033357600080fd5b6103496004808035906020019091905050611359565b005b341561035657600080fd5b6103c1600480803590602001909190803590602001908201803590602001908080601f016020809104026020016040519081016040528093929190818152602001838380828437820191505050505050919080359060200190919080359060200190919050506113ce565b005b34156103ce57600080fd5b6103e460048080359060200190919050506118a9565b005b34156103f157600080fd5b610453600480803590602001909190803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091908035906020019091905050611bc7565b005b341561046057600080fd5b61048c600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506120e0565b005b60008060008061049c612254565b60011515600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615151415156104fb57600080fd5b60008c11151561050a57600080fd5b60008b5111151561051a57600080fd5b60028a1115151561052a57600080fd5b60008911151561053957600080fd5b600073ffffffffffffffffffffffffffffffffffffffff168873ffffffffffffffffffffffffffffffffffffffff161415151561057557600080fd5b8494508473ffffffffffffffffffffffffffffffffffffffff16633235f04f8a6040518263ffffffff167c010000000000000000000000000000000000000000000000000000000002815260040180828152602001915050600060405180830381600087803b15156105e657600080fd5b6102c65a03f115156105f757600080fd5b505050600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1693508373ffffffffffffffffffffffffffffffffffffffff1663313ce5676000604051602001526040518163ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401602060405180830381600087803b151561068b57600080fd5b6102c65a03f1151561069c57600080fd5b50505060405180519050600a0a600b540292508373ffffffffffffffffffffffffffffffffffffffff1663313ce5676000604051602001526040518163ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401602060405180830381600087803b151561071b57600080fd5b6102c65a03f1151561072c57600080fd5b50505060405180519050600a0a8c0291508183018473ffffffffffffffffffffffffffffffffffffffff1663dd62ed3e33306000604051602001526040518363ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200192505050602060405180830381600087803b151561081757600080fd5b6102c65a03f1151561082857600080fd5b505050604051805190501015151561083f57600080fd5b8373ffffffffffffffffffffffffffffffffffffffff166323b872dd33308587016000604051602001526040518463ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018281526020019350505050602060405180830381600087803b151561092057600080fd5b6102c65a03f1151561093157600080fd5b505050604051805190505061016060405190810160405280600c54420181526020013373ffffffffffffffffffffffffffffffffffffffff1681526020018973ffffffffffffffffffffffffffffffffffffffff1681526020018881526020018781526020018c81526020018a8152602001838152602001848152602001600015158152602001600015158152509050600580548060010182816109d591906122e5565b91600052602060002090600a020160008390919091506000820151816000015560208201518160010160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060408201518160020160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550606082015181600301556080820151816004015560a0820151816005019080519060200190610ab3929190612317565b5060c0820151816006015560e0820151816007015561010082015181600801556101208201518160090160006101000a81548160ff0219169083151502179055506101408201518160090160016101000a81548160ff0219169083151502179055505050507f64ac6e05664ccea20b8f66ca8b0612bc21d8979c4c3ed8e99802b87956625683600160058054905003838d8d8d8d60405180878152602001868152602001806020018581526020018481526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001828103825286818151815260200191508051906020019080838360005b83811015610bd1578082015181840152602081019050610bb6565b50505050905090810190601f168015610bfe5780820380516001836020036101000a031916815260200191505b5097505050505050505060405180910390a1505050505050505050505050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610c7957600080fd5b80600760006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610d1857600080fd5b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482060405160405180910390a260008060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610e3f57600080fd5b80600a60006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610ede57600080fd5b60011515610eeb82612147565b1515141515610ef957600080fd5b600260008273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615600260008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff02191690831515021790555060011515600260008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff161515141561105b577fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff600360008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055505b50565b60011515600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615151415156110bd57600080fd5b7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205414151561112a57600080fd5b6000811215151561113a57600080fd5b600060058281548110151561114b57fe5b90600052602060002090600a02016006015411151561116957600080fd5b80600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055507fef97386272e1b95e98d066e75400adef65cf1abbb39f8873a7fc206cfc6f205c3382604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018281526020019250505060405180910390a150565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561127657600080fd5b80600960006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561131557600080fd5b80600860006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156113b457600080fd5b600b54811015156113c457600080fd5b80600b8190555050565b600060011515600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151514151561142f57600080fd5b84600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205414151561147c57600080fd5b6000845111151561148c57600080fd5b603c420182111580156114a25750603c42038210155b15156114ad57600080fd5b600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb336005888154811015156114fe57fe5b90600052602060002090600a0201600701546000604051602001526040518363ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b151561159d57600080fd5b6102c65a03f115156115ae57600080fd5b505050604051805190505060006005868154811015156115ca57fe5b90600052602060002090600a02016007018190555060028314156117515760016005868154811015156115f957fe5b90600052602060002090600a020160090160006101000a81548160ff0219169083151502179055508073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb600860009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1660058881548110151561166f57fe5b90600052602060002090600a0201600801546000604051602001526040518363ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b151561170e57600080fd5b6102c65a03f1151561171f57600080fd5b5050506040518051905050600060058681548110151561173b57fe5b90600052602060002090600a0201600801819055505b600160058681548110151561176257fe5b90600052602060002090600a020160090160016101000a81548160ff0219169083151502179055507fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055507f096a169a68f0172409609fe3bb771e0fca482be74c11ec5310e39956b0ecc74c858585856040518085815260200180602001848152602001838152602001828103825285818151815260200191508051906020019080838360005b8381101561186557808201518184015260208101905061184a565b50505050905090810190601f1680156118925780820380516001836020036101000a031916815260200191505b509550505050505060405180910390a15050505050565b600080426005848154811015156118bc57fe5b90600052602060002090600a020160000154101515156118db57600080fd5b3373ffffffffffffffffffffffffffffffffffffffff1660058481548110151561190157fe5b90600052602060002090600a020160010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1614151561195557600080fd5b600060058481548110151561196657fe5b90600052602060002090600a02016007015460058581548110151561198757fe5b90600052602060002090600a020160080154011115156119a657600080fd5b60009150600115156005848154811015156119bd57fe5b90600052602060002090600a020160090160019054906101000a900460ff1615151415611a385760006005848154811015156119f557fe5b90600052602060002090600a0201600801541115611a3357600583815481101515611a1c57fe5b90600052602060002090600a020160080154820191505b611a80565b600583815481101515611a4757fe5b90600052602060002090600a020160070154600584815481101515611a6857fe5b90600052602060002090600a02016008015483010191505b6000821115611b7657600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb33846000604051602001526040518363ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b1515611b5957600080fd5b6102c65a03f11515611b6a57600080fd5b50505060405180519050505b6000600584815481101515611b8757fe5b90600052602060002090600a0201600801819055506000600584815481101515611bad57fe5b90600052602060002090600a020160070181905550505050565b60008060011515600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff161515141515611c2957600080fd5b84600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054141515611c7657600080fd5b60008451111515611c8657600080fd5b603c42018311158015611c9c5750603c42038310155b1515611ca757600080fd5b600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1691508173ffffffffffffffffffffffffffffffffffffffff1663a9059cbb33600588815481101515611cf857fe5b90600052602060002090600a0201600701546000604051602001526040518363ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b1515611d9757600080fd5b6102c65a03f11515611da857600080fd5b50505060405180519050506000600586815481101515611dc457fe5b90600052602060002090600a020160070181905550600660009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508073ffffffffffffffffffffffffffffffffffffffff1663964edfd4600587815481101515611e2957fe5b90600052602060002090600a020160010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16600588815481101515611e6a57fe5b90600052602060002090600a020160020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16600589815481101515611eab57fe5b90600052602060002090600a020160030154876040518563ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001838152602001828152602001945050505050600060405180830381600087803b1515611f7c57600080fd5b6102c65a03f11515611f8d57600080fd5b5050506001600586815481101515611fa157fe5b90600052602060002090600a020160090160016101000a81548160ff0219169083151502179055507fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055507f3fca9087fa3bdd3547f902a585ff31d6706d9a276a95f29722e9659bf73105ae8585856040518084815260200180602001838152602001828103825284818151815260200191508051906020019080838360005b8381101561209d578082015181840152602081019050612082565b50505050905090810190601f1680156120ca5780820380516001836020036101000a031916815260200191505b5094505050505060405180910390a15050505050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561213b57600080fd5b6121448161215a565b50565b600080823b905060008111915050919050565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff161415151561219657600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a3806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6101606040519081016040528060008152602001600073ffffffffffffffffffffffffffffffffffffffff168152602001600073ffffffffffffffffffffffffffffffffffffffff16815260200160008152602001600081526020016122b8612397565b81526020016000815260200160008152602001600081526020016000151581526020016000151581525090565b81548183558181151161231257600a0281600a02836000526020600020918201910161231191906123ab565b5b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061235857805160ff1916838001178555612386565b82800160010185558215612386579182015b8281111561238557825182559160200191906001019061236a565b5b5090506123939190612480565b5090565b602060405190810160405280600081525090565b61247d91905b80821115612479576000808201600090556001820160006101000a81549073ffffffffffffffffffffffffffffffffffffffff02191690556002820160006101000a81549073ffffffffffffffffffffffffffffffffffffffff02191690556003820160009055600482016000905560058201600061243091906124a5565b6006820160009055600782016000905560088201600090556009820160006101000a81549060ff02191690556009820160016101000a81549060ff021916905550600a016123b1565b5090565b90565b6124a291905b8082111561249e576000816000905550600101612486565b5090565b90565b50805460018160011615610100020316600290046000825580601f106124cb57506124ea565b601f0160209004906000526020600020908101906124e99190612480565b5b505600a165627a7a723058206a049cc271121c6271c1b11e86b56d3962c7a8ffbccc6edc43f703f4b8a730020029";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final String FUNC_SETHASHCONTRACT = "setHashContract";

    public static final String FUNC_SETPRICECONTRACT = "setPriceContract";

    public static final String FUNC_SETTOKENSINK = "setTokenSink";

    public static final String FUNC_TOGGLEMPCGROUP = "toggleMPCGroup";

    public static final String FUNC_SETPAYMENTTOKEN = "setPaymentToken";

    public static final String FUNC_LOWERLOCKUPFEE = "lowerLockupFee";

    public static final String FUNC_ANALYZENEXTREQUEST = "analyzeNextRequest";

    public static final String FUNC_SUBMITREQUEST = "submitRequest";

    public static final String FUNC_ACCEPTREQUEST = "acceptRequest";

    public static final String FUNC_REJECTREQUEST = "rejectRequest";

    public static final String FUNC_WITHDRAWPAYMENT = "withdrawPayment";

    public static final Event NEXTREQUEST_EVENT = new Event("NextRequest", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Int256>() {}));
    ;

    public static final Event SUBMITREQUEST_EVENT = new Event("SubmitRequest", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event ACCEPTREQUEST_EVENT = new Event("AcceptRequest", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event REJECTREQUEST_EVENT = new Event("RejectRequest", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event OWNERSHIPRENOUNCED_EVENT = new Event("OwnershipRenounced", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}),
            Arrays.<TypeReference<?>>asList());
    ;

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
    }

    protected FuOPI(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected FuOPI(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String _newOwner) {
        final Function function = new Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new Address(_newOwner)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<FuOPI> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String _protocol, String _paymentToken, String _priceContract, String _hashes) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Address(_protocol),
                new Address(_paymentToken),
                new Address(_priceContract),
                new Address(_hashes)));
        return deployRemoteCall(FuOPI.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static RemoteCall<FuOPI> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String _protocol, String _paymentToken, String _priceContract, String _hashes) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Address(_protocol),
                new Address(_paymentToken),
                new Address(_priceContract),
                new Address(_hashes)));
        return deployRemoteCall(FuOPI.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public List<NextRequestEventResponse> getNextRequestEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(NEXTREQUEST_EVENT, transactionReceipt);
        ArrayList<NextRequestEventResponse> responses = new ArrayList<NextRequestEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            NextRequestEventResponse typedResponse = new NextRequestEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.mpc = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.nextToBeAnalyzed = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<NextRequestEventResponse> nextRequestEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, NextRequestEventResponse>() {
            @Override
            public NextRequestEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(NEXTREQUEST_EVENT, log);
                NextRequestEventResponse typedResponse = new NextRequestEventResponse();
                typedResponse.log = log;
                typedResponse.mpc = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.nextToBeAnalyzed = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<NextRequestEventResponse> nextRequestEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEXTREQUEST_EVENT));
        return nextRequestEventObservable(filter);
    }

    public List<SubmitRequestEventResponse> getSubmitRequestEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(SUBMITREQUEST_EVENT, transactionReceipt);
        ArrayList<SubmitRequestEventResponse> responses = new ArrayList<SubmitRequestEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            SubmitRequestEventResponse typedResponse = new SubmitRequestEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.position = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.payment = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.requestHash = (String) eventValues.getNonIndexedValues().get(2).getValue();
            typedResponse.mediaType = (BigInteger) eventValues.getNonIndexedValues().get(3).getValue();
            typedResponse.howManyPieces = (BigInteger) eventValues.getNonIndexedValues().get(4).getValue();
            typedResponse.user = (String) eventValues.getNonIndexedValues().get(5).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<SubmitRequestEventResponse> submitRequestEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, SubmitRequestEventResponse>() {
            @Override
            public SubmitRequestEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(SUBMITREQUEST_EVENT, log);
                SubmitRequestEventResponse typedResponse = new SubmitRequestEventResponse();
                typedResponse.log = log;
                typedResponse.position = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.payment = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.requestHash = (String) eventValues.getNonIndexedValues().get(2).getValue();
                typedResponse.mediaType = (BigInteger) eventValues.getNonIndexedValues().get(3).getValue();
                typedResponse.howManyPieces = (BigInteger) eventValues.getNonIndexedValues().get(4).getValue();
                typedResponse.user = (String) eventValues.getNonIndexedValues().get(5).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<SubmitRequestEventResponse> submitRequestEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(SUBMITREQUEST_EVENT));
        return submitRequestEventObservable(filter);
    }

    public List<AcceptRequestEventResponse> getAcceptRequestEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(ACCEPTREQUEST_EVENT, transactionReceipt);
        ArrayList<AcceptRequestEventResponse> responses = new ArrayList<AcceptRequestEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            AcceptRequestEventResponse typedResponse = new AcceptRequestEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.which = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.proof = (String) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AcceptRequestEventResponse> acceptRequestEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AcceptRequestEventResponse>() {
            @Override
            public AcceptRequestEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(ACCEPTREQUEST_EVENT, log);
                AcceptRequestEventResponse typedResponse = new AcceptRequestEventResponse();
                typedResponse.log = log;
                typedResponse.which = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.proof = (String) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AcceptRequestEventResponse> acceptRequestEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ACCEPTREQUEST_EVENT));
        return acceptRequestEventObservable(filter);
    }

    public List<RejectRequestEventResponse> getRejectRequestEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(REJECTREQUEST_EVENT, transactionReceipt);
        ArrayList<RejectRequestEventResponse> responses = new ArrayList<RejectRequestEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            RejectRequestEventResponse typedResponse = new RejectRequestEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.which = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.proof = (String) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.reason = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(3).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<RejectRequestEventResponse> rejectRequestEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, RejectRequestEventResponse>() {
            @Override
            public RejectRequestEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(REJECTREQUEST_EVENT, log);
                RejectRequestEventResponse typedResponse = new RejectRequestEventResponse();
                typedResponse.log = log;
                typedResponse.which = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.proof = (String) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.reason = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(3).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<RejectRequestEventResponse> rejectRequestEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(REJECTREQUEST_EVENT));
        return rejectRequestEventObservable(filter);
    }

    public List<OwnershipRenouncedEventResponse> getOwnershipRenouncedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, transactionReceipt);
        ArrayList<OwnershipRenouncedEventResponse> responses = new ArrayList<OwnershipRenouncedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipRenouncedEventResponse>() {
            @Override
            public OwnershipRenouncedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPRENOUNCED_EVENT, log);
                OwnershipRenouncedEventResponse typedResponse = new OwnershipRenouncedEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipRenouncedEventResponse> ownershipRenouncedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPRENOUNCED_EVENT));
        return ownershipRenouncedEventObservable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventObservable(filter);
    }

    public RemoteCall<TransactionReceipt> setHashContract(String _hashes) {
        final Function function = new Function(
                FUNC_SETHASHCONTRACT, 
                Arrays.<Type>asList(new Address(_hashes)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> setPriceContract(String _contract) {
        final Function function = new Function(
                FUNC_SETPRICECONTRACT, 
                Arrays.<Type>asList(new Address(_contract)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> setTokenSink(String sink) {
        final Function function = new Function(
                FUNC_SETTOKENSINK, 
                Arrays.<Type>asList(new Address(sink)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> toggleMPCGroup(String group) {
        final Function function = new Function(
                FUNC_TOGGLEMPCGROUP, 
                Arrays.<Type>asList(new Address(group)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> setPaymentToken(String token) {
        final Function function = new Function(
                FUNC_SETPAYMENTTOKEN, 
                Arrays.<Type>asList(new Address(token)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> lowerLockupFee(BigInteger lowerLockup) {
        final Function function = new Function(
                FUNC_LOWERLOCKUPFEE, 
                Arrays.<Type>asList(new Uint256(lowerLockup)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> analyzeNextRequest(BigInteger request) {
        final Function function = new Function(
                FUNC_ANALYZENEXTREQUEST, 
                Arrays.<Type>asList(new Int256(request)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> submitRequest(BigInteger tokenNumber, String requestHash, BigInteger mediaType, BigInteger howManyPieces, String user, BigInteger platformShare, BigInteger userShare) {
        final Function function = new Function(
                FUNC_SUBMITREQUEST, 
                Arrays.<Type>asList(new Uint256(tokenNumber),
                new Utf8String(requestHash),
                new Uint256(mediaType),
                new Uint256(howManyPieces),
                new Address(user),
                new Uint256(platformShare),
                new Uint256(userShare)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> acceptRequest(BigInteger which, String proof, BigInteger timestamp) {
        final Function function = new Function(
                FUNC_ACCEPTREQUEST, 
                Arrays.<Type>asList(new Int256(which),
                new Utf8String(proof),
                new Uint256(timestamp)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> rejectRequest(BigInteger which, String proof, BigInteger reason, BigInteger timestamp) {
        final Function function = new Function(
                FUNC_REJECTREQUEST, 
                Arrays.<Type>asList(new Int256(which),
                new Utf8String(proof),
                new Uint256(reason),
                new Uint256(timestamp)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> withdrawPayment(BigInteger which) {
        final Function function = new Function(
                FUNC_WITHDRAWPAYMENT, 
                Arrays.<Type>asList(new Uint256(which)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static FuOPI load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new FuOPI(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static FuOPI load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new FuOPI(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class NextRequestEventResponse {
        public Log log;

        public String mpc;

        public BigInteger nextToBeAnalyzed;
    }

    public static class SubmitRequestEventResponse {
        public Log log;

        public BigInteger position;

        public BigInteger payment;

        public String requestHash;

        public BigInteger mediaType;

        public BigInteger howManyPieces;

        public String user;
    }

    public static class AcceptRequestEventResponse {
        public Log log;

        public BigInteger which;

        public String proof;

        public BigInteger timestamp;
    }

    public static class RejectRequestEventResponse {
        public Log log;

        public BigInteger which;

        public String proof;

        public BigInteger reason;

        public BigInteger timestamp;
    }

    public static class OwnershipRenouncedEventResponse {
        public Log log;

        public String previousOwner;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }
}
