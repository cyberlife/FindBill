package org.tensorflow.demo.ethereum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.4.0.
 */
public class PlayerDatabase extends Contract {
    private static final String BINARY = "0x6060604052341561000f57600080fd5b33600460006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555061123b8061005f6000396000f30060606040526004361061008e576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680630616dbce146100935780630a9d793d146101305780631aadedd2146101695780633a7d280c146102065780636464d731146102dc57806367a4f8aa14610358578063edfa1fd6146103d4578063ef6b9fd8146104d6575b600080fd5b341561009e57600080fd5b6100ee600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190505061054b565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561013b57600080fd5b610167600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506105e0565b005b341561017457600080fd5b6101c4600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050610680565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561021157600080fd5b610261600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050610715565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156102a1578082015181840152602081019050610286565b50505050905090810190601f1680156102ce5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156102e757600080fd5b610356600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506108ed565b005b341561036357600080fd5b6103d2600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610ac7565b005b34156103df57600080fd5b6104d4600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050610ca1565b005b34156104e157600080fd5b610531600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050611081565b604051808215151515815260200191505060405180910390f35b60006001826040518082805190602001908083835b6020831015156105855780518252602082019150602081019050602083039250610560565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050919050565b600460009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561063c57600080fd5b80600560006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b60006002826040518082805190602001908083835b6020831015156106ba5780518252602082019150602081019050602083039250610695565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050919050565b61071d611156565b600080836040518082805190602001908083835b6020831015156107565780518252602082019150602081019050602083039250610731565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902080546001816001161561010002031660029004905011156108af576000826040518082805190602001908083835b6020831015156107dc57805182526020820191506020810190506020830392506107b7565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390208054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156108a35780601f10610878576101008083540402835291602001916108a3565b820191906000526020600020905b81548152906001019060200180831161088657829003601f168201915b505050505090506108e8565b6040805190810160405280600381526020017f307830000000000000000000000000000000000000000000000000000000000081525090505b919050565b600460009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561094957600080fd5b806002836040518082805190602001908083835b602083101515610982578051825260208201915060208101905060208303925061095d565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507f11369024d337dce7b2bc0a85d94a894538f40cfc3fc89c855bd7fc357f9ed621828260405180806020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001828103825284818151815260200191508051906020019080838360005b83811015610a88578082015181840152602081019050610a6d565b50505050905090810190601f168015610ab55780820380516001836020036101000a031916815260200191505b50935050505060405180910390a15050565b600460009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610b2357600080fd5b806001836040518082805190602001908083835b602083101515610b5c5780518252602082019150602081019050602083039250610b37565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507f2e925621aaf103759f2a43986f639470f4502a342600e8937d10a20b3b8edf54828260405180806020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001828103825284818151815260200191508051906020019080838360005b83811015610c62578082015181840152602081019050610c47565b50505050905090810190601f168015610c8f5780820380516001836020036101000a031916815260200191505b50935050505060405180910390a15050565b600460009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610cfd57600080fd5b826000856040518082805190602001908083835b602083101515610d365780518252602082019150602081019050602083039250610d11565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390209080519060200190610d7c92919061116a565b50816001856040518082805190602001908083835b602083101515610db65780518252602082019150602081019050602083039250610d91565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550816002826040518082805190602001908083835b602083101515610e605780518252602082019150602081019050602083039250610e3b565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507fca72a610c538f928f84a99debbfb2e2d483396ec9553ab16e683297d515534f9848484846040518080602001806020018573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001848103845288818151815260200191508051906020019080838360005b83811015610f70578082015181840152602081019050610f55565b50505050905090810190601f168015610f9d5780820380516001836020036101000a031916815260200191505b50848103835287818151815260200191508051906020019080838360005b83811015610fd6578082015181840152602081019050610fbb565b50505050905090810190601f1680156110035780820380516001836020036101000a031916815260200191505b50848103825285818151815260200191508051906020019080838360005b8381101561103c578082015181840152602081019050611021565b50505050905090810190601f1680156110695780820380516001836020036101000a031916815260200191505b5097505050505050505060405180910390a150505050565b60008073ffffffffffffffffffffffffffffffffffffffff166002836040518082805190602001908083835b6020831015156110d257805182526020820191506020810190506020830392506110ad565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1614151561114c5760019050611151565b600090505b919050565b602060405190810160405280600081525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106111ab57805160ff19168380011785556111d9565b828001600101855582156111d9579182015b828111156111d85782518255916020019190600101906111bd565b5b5090506111e691906111ea565b5090565b61120c91905b808211156112085760008160009055506001016111f0565b5090565b905600a165627a7a72305820fc8bb3420573cfb8fb76a6612138d85aa224a3528667939ce398245eceabb5700029";

    public static final String FUNC_SETPROTOCOL = "setProtocol";

    public static final String FUNC_FULLSIGNUP = "fullSignup";

    public static final String FUNC_EMAILASSIGNKEY = "emailAssignKey";

    public static final String FUNC_NUMBERASSIGNKEY = "numberAssignKey";

    public static final String FUNC_LOGIN = "login";

    public static final String FUNC_CHECKIFNUMBERISASSIGNED = "checkIfNumberIsAssigned";

    public static final String FUNC_GETADDRESSFROMEMAIL = "getAddressFromEmail";

    public static final String FUNC_GETADDRESSFROMNUMBER = "getAddressFromNumber";

    public static final Event ASSIGNKEYTOEMAIL_EVENT = new Event("AssignKeyToEmail", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event ASSIGNKEYTONUMBER_EVENT = new Event("AssignKeyToNumber", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event FULLSIGNUP_EVENT = new Event("FullSignUp", 
            Arrays.<TypeReference<?>>asList(),
            Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Address>() {}, new TypeReference<Utf8String>() {}));
    ;

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
    }

    protected PlayerDatabase(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected PlayerDatabase(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static RemoteCall<PlayerDatabase> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(PlayerDatabase.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<PlayerDatabase> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(PlayerDatabase.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public List<AssignKeyToEmailEventResponse> getAssignKeyToEmailEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(ASSIGNKEYTOEMAIL_EVENT, transactionReceipt);
        ArrayList<AssignKeyToEmailEventResponse> responses = new ArrayList<AssignKeyToEmailEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            AssignKeyToEmailEventResponse typedResponse = new AssignKeyToEmailEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.emailhash = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.assignedPublicKey = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssignKeyToEmailEventResponse> assignKeyToEmailEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssignKeyToEmailEventResponse>() {
            @Override
            public AssignKeyToEmailEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(ASSIGNKEYTOEMAIL_EVENT, log);
                AssignKeyToEmailEventResponse typedResponse = new AssignKeyToEmailEventResponse();
                typedResponse.log = log;
                typedResponse.emailhash = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.assignedPublicKey = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssignKeyToEmailEventResponse> assignKeyToEmailEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSIGNKEYTOEMAIL_EVENT));
        return assignKeyToEmailEventObservable(filter);
    }

    public List<AssignKeyToNumberEventResponse> getAssignKeyToNumberEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(ASSIGNKEYTONUMBER_EVENT, transactionReceipt);
        ArrayList<AssignKeyToNumberEventResponse> responses = new ArrayList<AssignKeyToNumberEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            AssignKeyToNumberEventResponse typedResponse = new AssignKeyToNumberEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.numberHash = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.assignedPublicKey = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssignKeyToNumberEventResponse> assignKeyToNumberEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssignKeyToNumberEventResponse>() {
            @Override
            public AssignKeyToNumberEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(ASSIGNKEYTONUMBER_EVENT, log);
                AssignKeyToNumberEventResponse typedResponse = new AssignKeyToNumberEventResponse();
                typedResponse.log = log;
                typedResponse.numberHash = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.assignedPublicKey = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssignKeyToNumberEventResponse> assignKeyToNumberEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSIGNKEYTONUMBER_EVENT));
        return assignKeyToNumberEventObservable(filter);
    }

    public List<FullSignUpEventResponse> getFullSignUpEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(FULLSIGNUP_EVENT, transactionReceipt);
        ArrayList<FullSignUpEventResponse> responses = new ArrayList<FullSignUpEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            FullSignUpEventResponse typedResponse = new FullSignUpEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.emailhash = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.passhash = (String) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.assignedPublicKey = (String) eventValues.getNonIndexedValues().get(2).getValue();
            typedResponse.phoneNumber = (String) eventValues.getNonIndexedValues().get(3).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<FullSignUpEventResponse> fullSignUpEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, FullSignUpEventResponse>() {
            @Override
            public FullSignUpEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(FULLSIGNUP_EVENT, log);
                FullSignUpEventResponse typedResponse = new FullSignUpEventResponse();
                typedResponse.log = log;
                typedResponse.emailhash = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.passhash = (String) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.assignedPublicKey = (String) eventValues.getNonIndexedValues().get(2).getValue();
                typedResponse.phoneNumber = (String) eventValues.getNonIndexedValues().get(3).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<FullSignUpEventResponse> fullSignUpEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(FULLSIGNUP_EVENT));
        return fullSignUpEventObservable(filter);
    }

    public RemoteCall<TransactionReceipt> setProtocol(String _fupow) {
        final Function function = new Function(
                FUNC_SETPROTOCOL, 
                Arrays.<Type>asList(new Address(_fupow)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> fullSignup(String emailhash, String passhash, String assignedPublicKey, String phoneNumber) {
        final Function function = new Function(
                FUNC_FULLSIGNUP, 
                Arrays.<Type>asList(new Utf8String(emailhash),
                new Utf8String(passhash),
                new Address(assignedPublicKey),
                new Utf8String(phoneNumber)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> emailAssignKey(String emailhash, String assignedPublicKey) {
        final Function function = new Function(
                FUNC_EMAILASSIGNKEY, 
                Arrays.<Type>asList(new Utf8String(emailhash),
                new Address(assignedPublicKey)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> numberAssignKey(String numberHash, String assignedPublicKey) {
        final Function function = new Function(
                FUNC_NUMBERASSIGNKEY, 
                Arrays.<Type>asList(new Utf8String(numberHash),
                new Address(assignedPublicKey)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> login(String emailhash) {
        final Function function = new Function(FUNC_LOGIN, 
                Arrays.<Type>asList(new Utf8String(emailhash)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<Boolean> checkIfNumberIsAssigned(String numberHash) {
        final Function function = new Function(FUNC_CHECKIFNUMBERISASSIGNED, 
                Arrays.<Type>asList(new Utf8String(numberHash)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<String> getAddressFromEmail(String emailhash) {
        final Function function = new Function(FUNC_GETADDRESSFROMEMAIL, 
                Arrays.<Type>asList(new Utf8String(emailhash)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<String> getAddressFromNumber(String numberHash) {
        final Function function = new Function(FUNC_GETADDRESSFROMNUMBER, 
                Arrays.<Type>asList(new Utf8String(numberHash)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public static PlayerDatabase load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new PlayerDatabase(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static PlayerDatabase load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new PlayerDatabase(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class AssignKeyToEmailEventResponse {
        public Log log;

        public String emailhash;

        public String assignedPublicKey;
    }

    public static class AssignKeyToNumberEventResponse {
        public Log log;

        public String numberHash;

        public String assignedPublicKey;
    }

    public static class FullSignUpEventResponse {
        public Log log;

        public String emailhash;

        public String passhash;

        public String assignedPublicKey;

        public String phoneNumber;
    }
}
