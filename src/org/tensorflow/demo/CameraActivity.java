/*
 * Copyright 2016 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.demo;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.Image.Plane;
import android.media.ImageReader;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Trace;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Random;

import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.ethereum.CommonToken;
import org.tensorflow.demo.ethereum.FuPoW;
import org.tensorflow.demo.ethereum.Stickers;
import org.tensorflow.demo.objects.CollectiblePopUp;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Hash;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.web3j.tx.Contract.GAS_LIMIT;
import static org.web3j.tx.ManagedTransaction.GAS_PRICE;

public abstract class CameraActivity extends Activity
        implements OnImageAvailableListener, Camera.PreviewCallback {
    private static final Logger LOGGER = new Logger();

    private static final int PERMISSIONS_REQUEST = 1;

    private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;

    private boolean debug = false;

    private Handler handler;
    private HandlerThread handlerThread;
    private boolean useCamera2API;
    private boolean isProcessingFrame = false;
    private byte[][] yuvBytes = new byte[3][];
    private int[] rgbBytes = null;
    private int yRowStride;

    protected int previewWidth = 0;
    protected int previewHeight = 0;

    private Runnable postInferenceCallback;
    private Runnable imageConverter;

    protected Web3j web3;

    protected Context context;

    protected StringBuilder alreadyDetected = new StringBuilder("");

    private Handler newLevelHandler;

    private Handler newLevelData;

    protected Animation fadeIn;

    protected String api = "http://www.convert-unix-time.com/api?timestamp=now";

    private CreateLevel createLevel;

    private UpdateDifficulty updateDifficulty;

    protected String random_object = null;

    protected int difficulty = 1;

    protected int currentlyFound = 0;

    protected BigInteger levelDeadline = BigInteger.valueOf(0);

    @BindView(R.id.found_collectible)
    ImageView collectible_image;

    @BindView(R.id.collectibleSurface)
    RelativeLayout collectibleSurface;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        LOGGER.d("onCreate " + this);
        super.onCreate(null);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_camera);

        ButterKnife.bind(this);

        context = this;

        updateDifficulty = new UpdateDifficulty();

        updateDifficulty.execute();

        collectibleSurface.bringToFront();

        collectible_image.bringToFront();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);

        if (hasPermission()) {

            setFragment();

            randomizeObject();

        } else {

            Toast.makeText(getApplicationContext(), "Camera permission was not granted",
                    Toast.LENGTH_LONG).show();

        }

        web3 = Web3jFactory.build(new HttpService("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

        newLevelHandler = new Handler();

        newLevelData = new Handler();

        newLevelHandler.postDelayed(new Runnable(){
            public void run(){

                createLevel = new CreateLevel();

                createLevel.execute();

                handler.postDelayed(this, 15000);
            }
        }, 10000);

        newLevelData.postDelayed(new Runnable(){
            public void run(){

                updateDifficulty = new UpdateDifficulty();

                updateDifficulty.execute();

                handler.postDelayed(this, 2000);
            }
        }, 2000);

    }

    private byte[] lastPreviewFrame;

    protected int[] getRgbBytes() {
        imageConverter.run();
        return rgbBytes;
    }

    protected int getLuminanceStride() {
        return yRowStride;
    }

    protected byte[] getLuminance() {
        return yuvBytes[0];
    }

    /**
     * Callback for android.hardware.Camera API
     */
    @Override
    public void onPreviewFrame(final byte[] bytes, final Camera camera) {
        if (isProcessingFrame) {
            LOGGER.w("Dropping frame!");
            return;
        }

        try {
            // Initialize the storage bitmaps once when the resolution is known.
            if (rgbBytes == null) {
                Camera.Size previewSize = camera.getParameters().getPreviewSize();

                Log.e("bill", "previewSize: " + previewSize.height + " " + previewSize.width);

                previewHeight = previewSize.height;
                previewWidth = previewSize.width;
                rgbBytes = new int[previewWidth * previewHeight];
                onPreviewSizeChosen(new Size(previewSize.width, previewSize.height), 90);
            }
        } catch (final Exception e) {
            LOGGER.e(e, "Exception!");
            return;
        }

        isProcessingFrame = true;
        lastPreviewFrame = bytes;
        yuvBytes[0] = bytes;
        yRowStride = previewWidth;

        imageConverter =
                new Runnable() {
                    @Override
                    public void run() {
                        ImageUtils.convertYUV420SPToARGB8888(bytes, previewWidth, previewHeight, rgbBytes);
                    }
                };

        postInferenceCallback =
                new Runnable() {
                    @Override
                    public void run() {
                        camera.addCallbackBuffer(bytes);
                        isProcessingFrame = false;
                    }
                };
        processImage();
    }

    /**
     * Callback for Camera2 API
     */
    @Override
    public void onImageAvailable(final ImageReader reader) {
        //We need wait until we have some size from onPreviewSizeChosen
        if (previewWidth == 0 || previewHeight == 0) {
            return;
        }
        if (rgbBytes == null) {
            rgbBytes = new int[previewWidth * previewHeight];
        }
        try {
            final Image image = reader.acquireLatestImage();

            if (image == null) {
                return;
            }

            if (isProcessingFrame) {
                image.close();
                return;
            }
            isProcessingFrame = true;
            Trace.beginSection("imageAvailable");
            final Plane[] planes = image.getPlanes();
            fillBytes(planes, yuvBytes);
            yRowStride = planes[0].getRowStride();
            final int uvRowStride = planes[1].getRowStride();
            final int uvPixelStride = planes[1].getPixelStride();

            imageConverter =
                    new Runnable() {
                        @Override
                        public void run() {
                            ImageUtils.convertYUV420ToARGB8888(
                                    yuvBytes[0],
                                    yuvBytes[1],
                                    yuvBytes[2],
                                    previewWidth,
                                    previewHeight,
                                    yRowStride,
                                    uvRowStride,
                                    uvPixelStride,
                                    rgbBytes);
                        }
                    };

            postInferenceCallback =
                    new Runnable() {
                        @Override
                        public void run() {
                            image.close();
                            isProcessingFrame = false;
                        }
                    };

            processImage();
        } catch (final Exception e) {
            LOGGER.e(e, "Exception!");
            Trace.endSection();
            return;
        }
        Trace.endSection();
    }

    @Override
    public synchronized void onStart() {
        LOGGER.d("onStart " + this);
        super.onStart();
    }

    @Override
    public synchronized void onResume() {
        LOGGER.d("onResume " + this);
        super.onResume();

        handlerThread = new HandlerThread("inference");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public synchronized void onPause() {
        // showAD();
        LOGGER.d("onPause " + this);

        if (!isFinishing()) {
            LOGGER.d("Requesting finish");
            finish();
        }

        handlerThread.quitSafely();
        try {
            handlerThread.join();
            handlerThread = null;
            handler = null;
        } catch (final InterruptedException e) {
            LOGGER.e(e, "Exception!");
        }

        super.onPause();
    }

    @Override
    public synchronized void onStop() {

        LOGGER.d("onStop " + this);

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("passHash", null);

        editor.putString("publicAddress", null);

        editor.apply();

        if (createLevel != null) {

            createLevel.cancel(true);

        }

        if (updateDifficulty != null) {

            updateDifficulty.cancel(true);

        }

        newLevelData.removeCallbacksAndMessages(null);

        newLevelHandler.removeCallbacksAndMessages(null);

        super.onStop();

    }

    @Override
    public synchronized void onDestroy() {
        LOGGER.d("onDestroy " + this);
        super.onDestroy();
    }

    protected String randomizeObject() {

        String[] concatenatedLabels = new String[1000];

        BufferedReader reader = null;

        int index = 0;

        try {

            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("coco_labels_list.txt")));

            String mLine;

            while ((mLine = reader.readLine()) != null) {

                concatenatedLabels[index] = mLine;

                index++;

            }

        } catch (IOException e) {
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        index--;

        String nextObject = concatenatedLabels[new Random().nextInt(concatenatedLabels.length)];

        while (nextObject.equals(random_object)) {

            nextObject = concatenatedLabels[new Random().nextInt(concatenatedLabels.length)];

        }

        Log.e("bill", "next object to find: " + nextObject.trim());

        return nextObject.trim();

    }

    protected synchronized void runInBackground(final Runnable r) {
        if (handler != null) {
            handler.post(r);
        }
    }

    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    // Returns true if the device supports the required hardware level, or better.
    private boolean isHardwareLevelSupported(
            CameraCharacteristics characteristics, int requiredLevel) {
        int deviceLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
            return requiredLevel == deviceLevel;
        }
        // deviceLevel is not LEGACY, can use numerical sort
        return requiredLevel <= deviceLevel;
    }

    private String chooseCamera() {
        final CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for (final String cameraId : manager.getCameraIdList()) {
                final CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

                // We don't use a front facing camera in this sample.
                final Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }

                final StreamConfigurationMap map =
                        characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                if (map == null) {
                    continue;
                }

                // Fallback to camera1 API for internal cameras that don't have full support.
                // This should help with legacy situations where using the camera2 API causes
                // distorted or otherwise broken previews.
                useCamera2API = (facing == CameraCharacteristics.LENS_FACING_EXTERNAL)
                        || isHardwareLevelSupported(characteristics,
                        CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL);
                LOGGER.i("Camera API lv2?: %s", useCamera2API);
                return cameraId;
            }
        } catch (CameraAccessException e) {
            LOGGER.e(e, "Not allowed to access camera");
        }

        return null;
    }

    protected void setFragment() {
        String cameraId = chooseCamera();

        Fragment fragment;
        if (useCamera2API) {
            CameraConnectionFragment camera2Fragment =
                    CameraConnectionFragment.newInstance(
                            new CameraConnectionFragment.ConnectionCallback() {
                                @Override
                                public void onPreviewSizeChosen(final Size size, final int rotation) {

                                    previewHeight = size.getHeight();
                                    previewWidth = size.getWidth();

                                    Log.e("bill", "set fragment sizes: " + previewHeight + " " + previewWidth);

                                    CameraActivity.this.onPreviewSizeChosen(size, rotation);

                                    collectible_image.bringToFront();


                                }
                            },
                            this,
                            getLayoutId(),
                            getDesiredPreviewFrameSize());

            camera2Fragment.setCamera(cameraId);
            fragment = camera2Fragment;
            // Toast.makeText(this, "newwwwwww", Toast.LENGTH_SHORT).show();
        } else {

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            //fragment = new LegacyCameraConnectionFragment(this, getLayoutId(),new Size(0,0));
            fragment = new LegacyCameraConnectionFragment(this, getLayoutId(), new Size(height, width));

            collectible_image.bringToFront();


        }

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

        collectible_image.bringToFront();

        collectibleSurface.bringToFront();


    }

    protected void fillBytes(final Plane[] planes, final byte[][] yuvBytes) {
        // Because of the variable row stride it's not possible to know in
        // advance the actual necessary dimensions of the yuv planes.
        for (int i = 0; i < planes.length; ++i) {
            final ByteBuffer buffer = planes[i].getBuffer();
            if (yuvBytes[i] == null) {
                LOGGER.d("Initializing buffer %d at size %d", i, buffer.capacity());
                yuvBytes[i] = new byte[buffer.capacity()];
            }
            buffer.get(yuvBytes[i]);
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public void requestRender() {
        final OverlayView overlay = (OverlayView) findViewById(R.id.debug_overlay);
        if (overlay != null) {
            overlay.postInvalidate();
        }
    }

    public void addCallback(final OverlayView.DrawCallback callback) {
        final OverlayView overlay = (OverlayView) findViewById(R.id.debug_overlay);
        if (overlay != null) {
            overlay.addCallback(callback);
        }
    }

    public void onSetDebug(final boolean debug) {
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            debug = !debug;
            requestRender();
            onSetDebug(debug);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void readyForNextImage() {
        if (postInferenceCallback != null) {
            postInferenceCallback.run();
        }
    }

    protected int getScreenOrientation() {
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_270:
                return 270;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_90:
                return 90;
            default:
                return 0;
        }
    }

    private class UpdateDifficulty extends AsyncTask<Void, Void, Void> {

        public UpdateDifficulty() {}

        @Override
        protected Void doInBackground(Void... params) {

            FuPoW pow = FuPoW.load(
                    "0xbb236378efac4f8097058856bf1d956b84c4e203", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            try {

                String timestampAPIResult = callAPI(api);

                String unixTimestamp = timestampAPIResult.split(",")[4]
                        .substring(timestampAPIResult.indexOf(":"));

                BigInteger newDeadline = pow.getCurrentRoundDeadline().send();

                if (new BigInteger(unixTimestamp).compareTo(levelDeadline) >= 0 &&
                        levelDeadline.compareTo(newDeadline) < 0) {

                    BigInteger newDifficulty = pow.getDifficulty().send();

                    difficulty = newDifficulty.intValue();

                    levelDeadline = newDeadline;

                    randomizeObject();

                    currentlyFound = 0;

                    Log.e("bill", "new deadline: " + String.valueOf(levelDeadline.intValue()));

                }

            } catch(Exception e) {



            }

            return null;

        }

    }

    private class CreateLevel extends AsyncTask<Void, Void, BigDecimal> {

        public CreateLevel() {

        }

        @Override
        protected BigDecimal doInBackground(Void... params) {

            SharedPreferences prefs = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);

            String address = prefs.getString("publicAddress", null);

            FuPoW pow = FuPoW.load(
                    "0xbb236378efac4f8097058856bf1d956b84c4e203", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            CommonToken common = CommonToken.load(
                    "0x2a2d85f4922becf09c2ed69313f23d9281e49ddc", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            try {

                Log.e("bill", String.valueOf(pow.isValid()));

                BigInteger end = pow.getCurrentRoundDeadline().send();

                Boolean foundAnswer = pow.getFoundCurrentAnswer().send();

                String timestampAPIResult = callAPI(api);

                BigInteger currentBalance = common.balanceOf(address).send();

                String unixTimestamp = timestampAPIResult.split(",")[4]
                        .substring(timestampAPIResult.indexOf(":"));

                if (new BigInteger(unixTimestamp).compareTo(end) >= 0 || foundAnswer == true) {

                    Log.e("bill", "creating level");

                    TransactionReceipt levelReceipt = pow.createNewLevel().send();

                    BigInteger updatedBalance = common.balanceOf(address).send();

                    BigInteger decimals = common.decimals().send();

                    BigDecimal adjustedBalance = new BigDecimal(updatedBalance.subtract(currentBalance));

                    if (updatedBalance.compareTo(currentBalance) > 0)
                        return adjustedBalance.divide(new BigDecimal(Math.pow(10, decimals.intValue())));

                    return BigDecimal.valueOf(0);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return BigDecimal.valueOf(0);

        }

        @Override
        protected void onPostExecute(BigDecimal result) {

            Log.e("bill", "level tokens " + result.intValue());

            if (result.intValue() > 0) {

                showCreatedLevelDialog(result.intValue());

            }

        }

    }

    private void showCreatedLevelDialog(int tokensGot) {

        new MaterialStyledDialog.Builder(this)
                .setTitle("You got money, honey")
                .setDescription("You received " + tokensGot + " tokens. Keep up scanning!")
                .setStyle(com.github.javiersantos.materialstyleddialogs.enums.Style.HEADER_WITH_ICON)
                .setIcon(R.drawable.tokens)
                .withDialogAnimation(true)
                .setHeaderColor(R.color.primary)
                .setCancelable(false)

                .setPositiveText("Gotcha")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {



                    }
                })
                .show();

    }

    private String callAPI(String url) {

        final String REQUEST_METHOD = "GET";
        final int READ_TIMEOUT = 2500;
        final int CONNECTION_TIMEOUT = 2500;

        String input;
        String result = "";

        try {

            URL myUrl = new URL(url);

            //Create a connection
            HttpURLConnection connection = (HttpURLConnection)
                    myUrl.openConnection();

            //Set methods and timeouts
            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);

            //Connect to our url
            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            while ((input = reader.readLine()) != null) {
                stringBuilder.append(input);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();

        } catch (Exception e) {

            Log.e("bill", String.valueOf(e.getStackTrace()));

            return "Error";

        }

        return result;

    }

    protected abstract void processImage();

    protected abstract void onPreviewSizeChosen(final Size size, final int rotation);

    protected abstract int getLayoutId();

    protected abstract Size getDesiredPreviewFrameSize();
}
