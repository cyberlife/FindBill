package org.tensorflow.demo.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.rootchecker.RootChecker;
import com.scottyab.rootbeer.RootBeer;

import org.tensorflow.demo.DetectorActivity;
import org.tensorflow.demo.R;
import org.tensorflow.demo.ethereum.FuPoW;
import org.tensorflow.demo.ethereum.PlayerDatabase;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Hash;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.http.HttpService;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


import static org.web3j.tx.Contract.GAS_LIMIT;
import static org.web3j.tx.ManagedTransaction.GAS_PRICE;

public class LoginActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST = 1;

    private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_login) Button _loginButton;
    @BindView(R.id.link_signup) TextView _signupLink;
    @BindView(R.id.switch_model_requests) TextView switchMode;

    private Web3j web3;

    private ProgressDialog progressDialog;

    private RootBeer rootBeer;

    private boolean isRooted;

    private String hashedLabels;

    private SharedPreferences prefs;

    private final String downloadPath = Environment.getExternalStoragePublicDirectory
                                  (Environment.DIRECTORY_DOWNLOADS).toString();

    private final String ipfsPath = "https://ipfs.io/ipfs/QmXgyWHzCMGcP1TFEbDPwheMe7qSdQrAVSNEQ1mmJhExCm";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //Init web3

        web3 = Web3jFactory.build(new HttpService("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

        //Check rooting and data integrity

        isRooted = checkRootedDevice();

        hashedLabels = checkIntegrity();

        //Check if a mode is set and if not set to centralized

        prefs = getApplicationContext().getSharedPreferences(
                "FindBill", Context.MODE_PRIVATE);

        if (prefs.getString("MODE", "nothing").equals("nothing") ||
                prefs.getString("MODE", "nothing").equals("CENTRALIZED")) {

            prefs.edit().putString("MODE", "CENTRALIZED").apply();

            switchMode.setText("Current mode: centralized. Click to switch");

        } else {

            switchMode.setText("Current mode: decentralized. Click to switch");

        }

        //Async check for rooting and labels

        new Checks(isRooted, hashedLabels).execute();

        //Button or Text onClick actions

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        switchMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefs = getApplicationContext().getSharedPreferences(
                        "FindBill", Context.MODE_PRIVATE);

                String mode = prefs.getString("MODE", "nothing");

                if (mode.equals("nothing") ||
                        !mode.equals("CENTRALIZED") && !mode.equals("DECENTRALIZED")) {

                    prefs.edit().putString("MODE", "CENTRALIZED").apply();

                    switchMode.setText("Current mode: centralized. Click to switch");

                } else if (mode.equals("CENTRALIZED")) {

                    prefs.edit().putString("MODE", "DECENTRALIZED").apply();

                    switchMode.setText("Current mode: decentralized. Click to switch");

                } else if (mode.equals("DECENTRALIZED")) {

                    prefs.edit().putString("MODE", "CENTRALIZED").apply();

                    switchMode.setText("Current mode: centralized. Click to switch");

                }

            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }

    private boolean checkRootedDevice() {

        rootBeer = new RootBeer(this);

        if (rootBeer.isRootedWithoutBusyBoxCheck()) {

            return true;

        }

        if (RootChecker.isDeviceRooted()) {

            return true;

        }

        return false;

    }

    public void login() {

        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed("Login failed");
            return;
        }

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        new BackgroundLogin(email, password, isRooted, hashedLabels).execute();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        finish();
    }

    private String checkIntegrity() {

        StringBuilder concatenatedLabels = new StringBuilder();

        BufferedReader reader = null;

        try {

            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("coco_labels_list.txt")));

            String mLine;

            while ((mLine = reader.readLine()) != null) {

                concatenatedLabels.append(mLine);

            }

        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        return Hash.sha3String(concatenatedLabels.toString());

    }

    public void onLoginSuccess(String email) {

        //CACHE EMAIL, CHECK MODE AND DECIDE APPROACH

        prefs = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("email", email);
        editor.apply();

        if (prefs.getString("MODE", "nothing").equals("CENTRALIZED")) {

            Intent myIntent = new Intent(LoginActivity.this, DetectorActivity.class);

            myIntent.putExtra("MODE", "CENTRALIZED");

            this.startActivity(myIntent);

            finish();

        } else if (prefs.getString("MODE", "nothing").equals("DECENTRALIZED")){

            new DownloadTask()
                    .execute(stringToURL(
                            ipfsPath
                    ));

        }

    }

    public void onLoginFailed(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("The password needs to have between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private class BackgroundLogin extends AsyncTask<Void, Void, String[]> {

        private String pass, email, labels;
        private boolean rooted;

        public BackgroundLogin(String email, String pass, boolean rooted, String hashedLabels) {

            this.email = email;
            this.pass = pass;
            this.rooted = rooted;
            this.labels = hashedLabels;

        }

        @Override
        protected String[] doInBackground(Void... params) {

            String[] checks = new String[3];

            checks[0] = checkingApp(rooted, labels);

            PlayerDatabase database = PlayerDatabase.load(
                    "0x52446e165c07b9ce869be1bac3521072908ae51d", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            try {

                Log.e("bill", String.valueOf(database.isValid()));

                String retrievePass = database.login(Hash.sha3String(email)).send();

                Log.e("bill", "both " + retrievePass + " " + Hash.sha3String(email));

                if (retrievePass.equals(Hash.sha3String(pass))) {

                    Log.e("bill", "pass ");

                    String address = database.getAddressFromEmail(Hash.sha3String(email)).send();

                    prefs = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString("passHash", Hash.sha3String(pass));

                    editor.putString("publicAddress", address);

                    editor.apply();

                    checks[1] = email;

                    return checks;

                } else {

                    checks[1] = "";

                    return checks;

                }

            } catch (Exception e) {
                Log.e("bill", e.toString());
            }

            checks[1] = "";

            return checks;


        }

        @Override
        protected void onPostExecute(String[] result) {

            Log.e("bill", "postExec " + result[0]);

            progressDialog.dismiss();

            if (result[0].equals("OK") && result[1].length() > 1) {

                onLoginSuccess(result[1]);

            } else {

                if (result[1].equals("ROOTED")) {

                    onLoginFailed("The device is rooted. Get an un-rooted device to mine coins");

                } else if (result[1].equals("LABELS")) {

                    onLoginFailed("Some app data is corrupted. Reinstall it from the Play Store");

                } else if (result[1].equals("MOCKED_LOCATION")) {

                    onLoginFailed("The device can spoof GPS data. Please remove GPS spoofing");

                }else {

                    onLoginFailed("The email or password is not correct");

                }

            }


        }

        @Override
        protected void onPreExecute() {

            showProgressDialog("Authenticating...");

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class Checks extends AsyncTask<Void, Void, String> {

        private String labels;
        private boolean rooted;

        public Checks(boolean rooted, String hashedLabels) {

            this.rooted = rooted;
            this.labels = hashedLabels;

        }

        @Override
        protected String doInBackground(Void... params) {

            File f = new File(downloadPath + "/findBill");

            if (!f.exists()) {

                f.mkdir();

            } else {

                f.delete();
                f.mkdir();

            }

            return checkingApp(rooted, labels);

        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equals("ROOTED")){

                Toast.makeText(getApplicationContext(), "The device is rooted. Get an un-rooted device to mine coins.",
                        Toast.LENGTH_LONG).show();

                finish();

            } else if (result.equals("MOCKED_LOCATION")) {

                Toast.makeText(getApplicationContext(), "Your phone has GPS spoofing apps/permissions. Disable them and retry",
                        Toast.LENGTH_LONG).show();

                Toast.makeText(getApplicationContext(), "Your phone has GPS spoofing apps/permissions. Disable them and retry",
                        Toast.LENGTH_LONG).show();

                finish();

            } else if (result.equals("LABELS")) {

                Toast.makeText(getApplicationContext(), "Some app data is corrupted. Reinstall it from the Play Store.",
                        Toast.LENGTH_LONG).show();

                finish();

            } else if (result.equals("OK")) {

                if (!hasPermission()) {

                    requestPermission();

                }

            }

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(
            final int requestCode, final String[] permissions, final int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                Toast.makeText(getApplicationContext(), "This app needs camera permission",
                            Toast.LENGTH_LONG).show();

                finish();
            }
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(PERMISSION_CAMERA)) {
                Toast.makeText(LoginActivity.this,
                        "Camera and storage permission are required", Toast.LENGTH_LONG).show();
            }
            requestPermissions(new String[]{PERMISSION_CAMERA}, PERMISSIONS_REQUEST);
        }
    }

    private String checkingApp(boolean rooted, String labels) {

        if (rooted) return "ROOTED";

        if (areThereMockPermissionApps(this)) return "MOCKED_LOCATION";

        FuPoW fupow = FuPoW.load(
                "0xbb236378efac4f8097058856bf1d956b84c4e203", web3,
                Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                GAS_PRICE, GAS_LIMIT);

        try {

            Log.e("bill", String.valueOf(fupow.isValid()));

          /*  String correctHashedLabels = fupow.getHashedLabels().send();

            if (!correctHashedLabels.equals(labels)) {

                return "LABELS";

            } */

        } catch (Exception e) {
            Log.e("bill", e.toString());
        }

        return "OK";

    }

    private void showProgressDialog(String message) {

        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();

    }

    private class DownloadTask extends AsyncTask<URL,Void,Bitmap>{
        // Before the tasks execution
        protected void onPreExecute(){

            showProgressDialog("Setting up the camera...");

        }

        // Do the task in background/non UI thread
        protected Bitmap doInBackground(URL...urls){
            URL url = urls[0];
            HttpURLConnection connection = null;

            try{
                // Initialize a new http url connection
                connection = (HttpURLConnection) url.openConnection();

                // Connect the http url connection
                connection.connect();

                // Get the input stream from http url connection
                InputStream inputStream = connection.getInputStream();

                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

                // Convert BufferedInputStream to Bitmap object
                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);

                // Return the downloaded bitmap
                return bmp;

            }catch(IOException e){
                e.printStackTrace();
            }finally{
                // Disconnect the http url connection
                connection.disconnect();
            }
            return null;
        }

        // When all async task done
        protected void onPostExecute(Bitmap result){

            if(result!=null){

                // Save bitmap to internal storage
                Uri imageInternalUri = saveImageToInternalStorage(downloadPath, result);

                Log.e("bill", imageInternalUri.toString());

                Intent myIntent = new Intent(LoginActivity.this, DetectorActivity.class);

                myIntent.putExtra("MODE", "DECENTRALIZED");

                startActivity(myIntent);

                finish();

            }else {

                Log.e("bill", "could not download file");

            }

            progressDialog.dismiss();

        }
    }

    // Custom method to convert string to url
    protected URL stringToURL(String urlString){
        try{
            URL url = new URL(urlString);
            return url;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return null;
    }

    // Custom method to save a bitmap into internal storage
    protected Uri saveImageToInternalStorage(String downloadPath, Bitmap bitmap){
        // Initialize ContextWrapper
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());

        // Initializing a new file
        // The bellow line return a directory in internal storage
        File file = wrapper.getDir("Images",MODE_PRIVATE);

        if (!file.exists()) file.mkdir();

        // Create a file to save the image
        file = new File(file, "cat.jpg");

        try{
            // Initialize a new OutputStream
            OutputStream stream = null;

            // If the output file exists, it can be replaced or appended to it
            stream = new FileOutputStream(file);

            // Compress the bitmap
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

            // Flushes the stream
            stream.flush();

            // Closes the stream
            stream.close();

        }catch (IOException e) // Catch the exception
        {
            e.printStackTrace();
        }

        // Parse the gallery image url to uri
        Uri savedImageURI = Uri.parse(file.getAbsolutePath());

        // Return the saved image Uri
        return savedImageURI;
    }

    public static boolean areThereMockPermissionApps(Context context) {
        int count = 0;

        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages =
                pm.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo applicationInfo : packages) {
            try {
                PackageInfo packageInfo = pm.getPackageInfo(applicationInfo.packageName,
                        PackageManager.GET_PERMISSIONS);

                // Get Permissions
                String[] requestedPermissions = packageInfo.requestedPermissions;

                if (requestedPermissions != null) {
                    for (int i = 0; i < requestedPermissions.length; i++) {
                        if (requestedPermissions[i]
                                .equals("android.permission.ACCESS_MOCK_LOCATION")
                                && !applicationInfo.packageName.equals(context.getPackageName())) {
                            count++;
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("Got exception " , e.getMessage());
            }
        }

        if (count > 0)
            return true;
        return false;
    }

}
