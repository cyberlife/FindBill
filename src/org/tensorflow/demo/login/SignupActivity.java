package org.tensorflow.demo.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.tensorflow.demo.DetectorActivity;
import org.tensorflow.demo.R;

import org.tensorflow.demo.ethereum.ClientManager;
import org.tensorflow.demo.ethereum.PlayerDatabase;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Hash;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Future;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.web3j.tx.Contract.GAS_LIMIT;
import static org.web3j.tx.ManagedTransaction.GAS_PRICE;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @BindView(R.id.btn_signup) Button _signupButton;
    @BindView(R.id.link_login) TextView _loginLink;
    @BindView(R.id.switch_model_requests) TextView switchMode;
    @BindView(R.id.publicAddr) EditText publicAddr;

    ProgressDialog progressDialog;

    private Web3j web3;

    private SharedPreferences prefs;

    private final String downloadPath = Environment.getExternalStoragePublicDirectory
            (Environment.DIRECTORY_DOWNLOADS).toString();

    private final String ipfsPath = "https://ipfs.io/ipfs/QmXgyWHzCMGcP1TFEbDPwheMe7qSdQrAVSNEQ1mmJhExCm";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        web3 = Web3jFactory.build(new HttpService("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

        //Check if a mode is set and if not set to centralized

        prefs = getApplicationContext().getSharedPreferences(
                "FindBill", Context.MODE_PRIVATE);

        if (prefs.getString("MODE", "nothing").equals("nothing") ||
                prefs.getString("MODE", "nothing").equals("CENTRALIZED")) {

            prefs.edit().putString("MODE", "CENTRALIZED").apply();

            switchMode.setText("Current mode: centralized. Click to switch");

        } else {

            switchMode.setText("Current mode: decentralized. Click to switch");

        }

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        switchMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefs = getApplicationContext().getSharedPreferences(
                        "FindBill", Context.MODE_PRIVATE);

                String mode = prefs.getString("MODE", "nothing");

                if (mode.equals("nothing") ||
                        !mode.equals("CENTRALIZED") && !mode.equals("DECENTRALIZED")) {

                    prefs.edit().putString("MODE", "CENTRALIZED").apply();

                    switchMode.setText("Current mode: centralized. Click to switch");

                } else if (mode.equals("CENTRALIZED")) {

                    prefs.edit().putString("MODE", "DECENTRALIZED").apply();

                    switchMode.setText("Current mode: decentralized. Click to switch");

                } else if (mode.equals("DECENTRALIZED")) {

                    prefs.edit().putString("MODE", "CENTRALIZED").apply();

                    switchMode.setText("Current mode: centralized. Click to switch");

                }

            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        String address = publicAddr.getText().toString();

        new BackgroundSignUp(email, password, address).execute();

    }

    private String checkIntegrity() {

        StringBuilder concatenatedLabels = new StringBuilder();

        BufferedReader reader = null;

        try {

            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("coco_labels_list.txt")));

            String mLine;

            while ((mLine = reader.readLine()) != null) {

                concatenatedLabels.append(mLine);

            }

        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        return Hash.sha3String(concatenatedLabels.toString());

    }


    public void onSignupSuccess(String email) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString("email", email);
        editor.apply();

        if (prefs.getString("MODE", "nothing").equals("CENTRALIZED")) {

            Intent myIntent = new Intent(SignupActivity.this, DetectorActivity.class);

            myIntent.putExtra("MODE", "CENTRALIZED");

            this.startActivity(myIntent);

            finish();

        } else if (prefs.getString("MODE", "nothing").equals("DECENTRALIZED")){

            new DownloadTask()
                    .execute(stringToURL(
                            ipfsPath
                    ));

        }

    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Could not sign up", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        finish();
    }

    public boolean validate() {
        boolean valid = true;


        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("Between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (!(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError("Passwords do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        if (publicAddr.getText().length() < 30) {
            publicAddr.setError("Write a valid address");
            valid = false;
        } else {
            publicAddr.setError(null);
        }

        return valid;
    }

    private class BackgroundSignUp extends AsyncTask<Void, Void, String> {

        private String pass, email, publicAddress;

        public BackgroundSignUp(String email, String pass, String publicAddress) {

            this.email = email;
            this.pass = pass;
            this.publicAddress = publicAddress;

        }

        @Override
        protected String doInBackground(Void... params) {

            PlayerDatabase database = PlayerDatabase.load(
                    "0x52446e165c07b9ce869be1bac3521072908ae51d", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            ClientManager client = ClientManager.load(
                    "0xe3c0398b4127dff2cc396a09d243341d6bf33785", web3,
                    Credentials.create("5bf4843a007d5ebb0ac480a06ea1981318120090881ab42aac3547b6afb6589c"),
                    GAS_PRICE, GAS_LIMIT);

            try {

                Log.e("bill", String.valueOf(database.isValid()));

                Log.e("bill", "email given: " + email + " passGiven " + pass);

                String passwordHash = database.login(Hash.sha3String(email)).send();

                Log.e("bill", "passHash for given email: " + passwordHash);

                if (!passwordHash.equals("0x0")) {return "";}

                TransactionReceipt transactionReceipt = database.fullSignup(
                        Hash.sha3String(email),
                        Hash.sha3String(pass),
                        publicAddress,
                        "0x0"
                ).send();

                Log.e("bill", "given email hash " + Hash.sha3String(email) + " given pass hash: "
                        + Hash.sha3String(pass));

                Future<TransactionReceipt> clientReceipt = client.raiseMintLimit(publicAddress, "signUp").sendAsync();

            } catch (Exception e) {
                Log.e("bill", e.toString());

                return "";

            }

            SharedPreferences pref = getApplicationContext().getSharedPreferences("FindBill", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putString("passHash", Hash.sha3String(pass));

            editor.putString("publicAddress", publicAddress);

            editor.apply();

            return email;

        }

        @Override
        protected void onPostExecute(String result) {

            progressDialog.dismiss();

            if (result.length() <= 1) {

                onSignupFailed();

            } else {

                onSignupSuccess(result);

            }

        }

        @Override
        protected void onPreExecute() {

            progressDialog = new ProgressDialog(SignupActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creating Account...");
            progressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void showProgressDialog(String message) {

        progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();

    }

    private class DownloadTask extends AsyncTask<URL,Void,Bitmap>{
        // Before the tasks execution
        protected void onPreExecute(){

            showProgressDialog("Setting up the camera...");

        }

        // Do the task in background/non UI thread
        protected Bitmap doInBackground(URL...urls){
            URL url = urls[0];
            HttpURLConnection connection = null;

            try{
                // Initialize a new http url connection
                connection = (HttpURLConnection) url.openConnection();

                // Connect the http url connection
                connection.connect();

                // Get the input stream from http url connection
                InputStream inputStream = connection.getInputStream();

                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

                // Convert BufferedInputStream to Bitmap object
                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);

                // Return the downloaded bitmap
                return bmp;

            }catch(IOException e){
                e.printStackTrace();
            }finally{
                // Disconnect the http url connection
                connection.disconnect();
            }
            return null;
        }

        // When all async task done
        protected void onPostExecute(Bitmap result){

            if(result!=null){

                // Save bitmap to internal storage
                Uri imageInternalUri = saveImageToInternalStorage(downloadPath, result);

                Log.e("bill", imageInternalUri.toString());

                Intent myIntent = new Intent(SignupActivity.this, DetectorActivity.class);

                myIntent.putExtra("MODE", "DECENTRALIZED");

                startActivity(myIntent);

                finish();

            }else {

                Log.e("bill", "could not download file");

            }

            progressDialog.dismiss();

        }
    }

    // Custom method to convert string to url
    protected URL stringToURL(String urlString){
        try{
            URL url = new URL(urlString);
            return url;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return null;
    }

    // Custom method to save a bitmap into internal storage
    protected Uri saveImageToInternalStorage(String downloadPath, Bitmap bitmap){
        // Initialize ContextWrapper
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());

        // Initializing a new file
        // The bellow line return a directory in internal storage
        File file = wrapper.getDir("Images",MODE_PRIVATE);

        if (!file.exists()) file.mkdir();

        // Create a file to save the image
        file = new File(file, "cat.jpg");

        try{
            // Initialize a new OutputStream
            OutputStream stream = null;

            // If the output file exists, it can be replaced or appended to it
            stream = new FileOutputStream(file);

            // Compress the bitmap
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

            // Flushes the stream
            stream.flush();

            // Closes the stream
            stream.close();

        }catch (IOException e) // Catch the exception
        {
            e.printStackTrace();
        }

        // Parse the gallery image url to uri
        Uri savedImageURI = Uri.parse(file.getAbsolutePath());

        // Return the saved image Uri
        return savedImageURI;
    }

}