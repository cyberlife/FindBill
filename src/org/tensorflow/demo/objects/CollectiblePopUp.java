package org.tensorflow.demo.objects;

public class CollectiblePopUp {

    private String objectName;

    private String objectHash;

    private int objectPrice;

    public CollectiblePopUp(String name, String hash, int object) {

        objectName = name;
        objectHash = hash;
        objectPrice = object;

    }

    public String getObjectHash() {
        return objectHash;
    }

    public void setObjectHash(String objectHash) {
        this.objectHash = objectHash;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public int getObjectPrice() {
        return objectPrice;
    }

    public void setObjectPrice(int objectPrice) {
        this.objectPrice = objectPrice;
    }

}
