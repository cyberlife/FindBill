// Generated code from Butter Knife. Do not modify!
package org.tensorflow.demo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CameraActivity_ViewBinding implements Unbinder {
  private CameraActivity target;

  @UiThread
  public CameraActivity_ViewBinding(CameraActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CameraActivity_ViewBinding(CameraActivity target, View source) {
    this.target = target;

    target.collectible_image = Utils.findRequiredViewAsType(source, R.id.found_collectible, "field 'collectible_image'", ImageView.class);
    target.collectibleSurface = Utils.findRequiredViewAsType(source, R.id.collectibleSurface, "field 'collectibleSurface'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CameraActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.collectible_image = null;
    target.collectibleSurface = null;
  }
}
